from py2neo import neo4j
from py2neo import cypher
import random
import os

graph_db = neo4j.GraphDatabaseService("http://localhost:7474/db/data/")
neo4j._add_header('X-Stream', 'true;format=pretty')

def main():
    
    tag_idx = graph_db.get_or_create_index(neo4j.Node, "tag_idx")
    cnt = 0
    query = neo4j.CypherQuery(graph_db, "MATCH (n:`tag`) RETURN n")
    for record in query.execute():
        tag = record[0]["value"].encode('utf-8').strip();
        parsed_tag = tag.split(" ")
        for term in parsed_tag:
            cleaned_term = term.lower()
            cleaned_term = ''.join(e for e in cleaned_term if e.isalnum())
            tag_idx.add("value", cleaned_term, record[0])
        cnt+=1
        if cnt%100==0: print str(cnt)+ " tag nodes indexed."
    print str(cnt)+ " tag nodes indexed."

    director_idx = graph_db.get_or_create_index(neo4j.Node, "director_idx")
    cnt = 0
    query = neo4j.CypherQuery(graph_db, "MATCH (n:`director`) RETURN n")
    for record in query.execute():
        director_name = record[0]["directorName"].encode('utf-8').strip();
        parsed_director_name = director_name.split(" ")
        for term in parsed_director_name:
            cleaned_term = term.lower()
            cleaned_term = ''.join(e for e in cleaned_term if e.isalnum())
            director_idx.add("directorName", cleaned_term, record[0])
        cnt+=1
        if cnt%100==0: print str(cnt)+ " director nodes indexed."
    print str(cnt)+ " director nodes indexed."
    
    genre_idx = graph_db.get_or_create_index(neo4j.Node, "genre_idx")
    cnt = 0
    query = neo4j.CypherQuery(graph_db, "MATCH (n:`genre`) RETURN n")
    for record in query.execute():
        genre = record[0]["genre"].encode('utf-8').strip();
        parsed_genre = genre.split(" ")
        for term in parsed_genre:
            cleaned_term = term.lower()
            cleaned_term = ''.join(e for e in cleaned_term if e.isalnum())
            genre_idx.add("genre", cleaned_term, record[0])
        cnt+=1
        if cnt%100==0: print str(cnt)+ " genre nodes indexed."
    print str(cnt)+ " genre nodes indexed."

    country_idx = graph_db.get_or_create_index(neo4j.Node, "country_idx")
    cnt = 0
    query = neo4j.CypherQuery(graph_db, "MATCH (n:`country`) RETURN n")
    for record in query.execute():
        country = record[0]["country"].encode('utf-8').strip();
        parsed_country = country.split(" ")
        for term in parsed_country:
            cleaned_term = term.lower()
            cleaned_term = ''.join(e for e in cleaned_term if e.isalnum())
            country_idx.add("country", cleaned_term, record[0])
        cnt+=1
        if cnt%100==0: print str(cnt)+ " country nodes indexed."
    print str(cnt)+ " country nodes indexed."
    
    actor_idx = graph_db.get_or_create_index(neo4j.Node, "actor_idx")
    cnt = 0
    query = neo4j.CypherQuery(graph_db, "MATCH (n:`actor`) RETURN n")
    for record in query.execute():
        try:
            actor_name = record[0]["actorName"].encode('utf-8').strip();
            parsed_actor_name = actor_name.split(" ")
            for term in parsed_actor_name:
                cleaned_term = term.lower()
                cleaned_term = ''.join(e for e in cleaned_term if e.isalnum())
                actor_idx.add("actorName", cleaned_term, record[0])
        except:
            pass
        cnt+=1
        if cnt%100==0: print str(cnt)+ " actor nodes indexed."
    print str(cnt)+ " actor nodes indexed."

    movie_idx = graph_db.get_or_create_index(neo4j.Node, "movie_idx")
    cnt = 0
    query = neo4j.CypherQuery(graph_db, "MATCH (n:`movie`) RETURN n")
    for record in query.execute():
        movie_title = record[0]["title"].encode('utf-8').strip();
        parsed_title = movie_title.split(" ")
        for term in parsed_title:
            cleaned_term = term.lower()
            cleaned_term = ''.join(e for e in cleaned_term if e.isalnum())
            movie_idx.add("title", cleaned_term, record[0])
        cnt+=1
        if cnt%100==0: print str(cnt)+ " movie nodes indexed."
    print str(cnt)+ " movie nodes indexed."
    
    user_idx = graph_db.get_or_create_index(neo4j.Node, "user_idx")
    cnt = 0
    query = neo4j.CypherQuery(graph_db, "MATCH (n:`user`) RETURN n")
    for record in query.execute():
        user_idx.add("username", record[0]["userID"].encode('utf-8').strip(), record[0])
        cnt+=1
        if cnt%100==0: print str(cnt)+ " user nodes indexed."
    print str(cnt)+ " user nodes indexed."
                                   
if __name__=="__main__":
    main()
