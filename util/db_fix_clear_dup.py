from py2neo import neo4j
from py2neo import cypher
import threading
import operator
from multiprocessing import Process, Queue

graph_db = neo4j.GraphDatabaseService("http://localhost:7474/db/data/")
neo4j._add_header('X-Stream', 'true;format=pretty')

def main():

    query_str = """
        MATCH (m:movie)
        WITH m.imdbID as m_imdb, count(m) as cnt
        where cnt!=1
        RETURN m_imdb, cnt
        order by cnt desc
    """
    
    query = neo4j.CypherQuery(graph_db, query_str)
    for record in query.execute():
        m_imdb = record[0]
        cnt = record[1]
        query_str_1 = """
        
        MATCH (m:movie)
        WHERE m.imdbID='"""+m_imdb+"""'
        RETURN m
        
        """
        m_list = []
        query_1 = neo4j.CypherQuery(graph_db, query_str_1)
        for record_1 in query_1.execute():
            m = record_1[0]
            m_list.append(m)
        
        repre_m = m_list[0]
        other_ms = m_list[1:]    
        print repre_m._id
        for one_m in other_ms:
            print "+" + str(one_m._id)
            
            rels = list(graph_db.match(start_node=one_m))
            for rel in rels:
                rels_test = list(graph_db.match(start_node=repre_m, end_node = rel.end_node, rel_type = rel.type))
                if len(rels_test)>0:
                    #print "exist : " + str(rel)
                    pass
                else:
                    graph_db.create((repre_m, rel.type, rel.end_node,rel.get_properties()))
                
                graph_db.delete(rel)    
                       
            rels = list(graph_db.match(end_node=one_m))
            for rel in rels:
                rels_test = list(graph_db.match(start_node=rel.start_node, end_node = repre_m, rel_type = rel.type))
                if len(rels_test)>0:
                    #print "exist : " + str(rel)
                    pass
                else:
                    graph_db.create((rel.start_node, rel.type, repre_m,rel.get_properties()))
                
                graph_db.delete(rel)
                
            graph_db.delete(one_m)
                                
if __name__=="__main__":
    main()
