\section{PRECOS:System Architecture and Functionalities}
In this section, we describe an overall architecture and the main functionalities of {\em PRECOS}.

\subsection{System Architecture}
\begin{figure}[ht]
\center
\includegraphics[width=0.45\textwidth]{./figures/precos-architecture}
\centering\caption{The Three Layer Architecture of PRECOS: Bootstrap framework
(http://getbootstrap.com), Tornado Web Server (http://www.tornadoweb.org) ,
Neo4j Graph Database (http://www.neo4j.org/) were mainly used for implementation
of \textit{User Interface Layer}, \textit{Data Processing Layer}, and
\textit{Database Layer respectively.}}
\label{fig:overview}
\end{figure}

As shown in Fig.~\ref{fig:overview}, {\em PRECOS} is composed of three
layers.  (1) The \textit{User Interface Layer}, a web-based graphical
user interface (GUI), converts user interactions into AJAX queries
that are sent to the \textit{Data Processing Layer} for the retrieval
of data for the next step, and updates the contents presented to the
user once the results are returned from the \textit{Data Processing Layer}. 
(2) \textit{Data Processing Layer}, on receiving AJAX queries 
from the \textit{user interface layer}, reformulates the queries into 
\textit{Cypher}\footnote{http://docs.neo4j.org/chunked/stable/cypher-introduction.html},
a native query language for the backend database, and sends them to \textit{Database Layer} for processing the requests. 
(3) The \textit{Database Layer} manages a graph representation of the movie data, and processes
queries requests from the \textit{Data Processing Layer}. 

Two datasets are used to construct the movie graph database, 
HetRec 2011 dataset \cite{HetRec2011} which is an extension of MovieLens10M dataset joined with
Internet Movie Database (IMDb) \footnote{http://www.imdb.com} and Rotten
Tomatoes movie review systems\footnote{http://www.rottentomatoes.com}. The 
graph is \textit{heterogeneous} in that it is composed of nine node types (\textit{e.g.}, $\small{\textsf{USER}}$, $\small{\textsf{MOVIE}}$,
$\small{\textsf{ACTOR}}$, $\small{\textsf{DIRECTOR}}$, $\small{\textsf{TAG}}$,
\ldots) and eleven edge types (\textit{e.g.}, $\small{\textsf{USER}}$
$\mathcal{\xrightarrow{\mathrm{\textit{liked}}}}$ $\small{\textsf{MOVIE}}$, $\small{\textsf{MOVIE}}$ $\mathcal{\xrightarrow{\mathrm{\textit{directedBy}}}}$
$\small{\textsf{DIRECTOR}}$, $\small{\textsf{MOVIE}}$
$\mathcal{\xrightarrow{\mathrm{\textit{starring}}}}$ $\small{\textsf{ACTOR}}$,
\ldots). The constructed graph is stored in Neo4j graph database \cite{neo4j2013} for efficient link-oriented query processing. In the next
section, we explain PRECOS's main functionalities and the underlying algorithms.

\subsection{Functionalities}

{\em PRECOS} is a web-based application that offers three
functionalities, search and browsing, and recommendation to users.  As
illustrated in Fig.~\ref{fig:gui}, the interface is mainly composed of
two parts; menu bar (Fig. 3 (A)) and the movie browser (Fig. 3
(B)). In addition, three auxiliary parts (Fig. 3 (C), (D), and
(E)) are placed within the parts. Users interacts with these five parts
to locate their preferable movies.

After the login, popular movies that have not been rated by the user
are presented in the movie browser on user's convenience sake. Users,
of course, can choose a different set of movies to start a selection
by a separate search.

\begin{figure}[ht]
\center
\includegraphics[width=0.45\textwidth]{./figures/movie-browser}
\centering\caption{The User Interface of PRECOS; (A) Menu Bar (B) Movie Browser,
(C) Search box, (D) Navigation Menu, (E) Recommendation Menu}
\label{fig:gui}
\end{figure}

\noindent
\textbf{\underline{Keyword Search:}} PRECOS provides a fast keyword
search by indexing all meta-information of each movie. Users can
retrieve movies of interest simply by typing keywords as the search
terms. For example, let us assume that a user is interested in horror
movies. If the user types `horror' in the search box (Fig. 3 (C)) and
clicks the submit button, all movies, actors, and the other types of
entities containing the given keyword `horror' will be shown in the
movie browser.

\noindent
\textbf{\underline{Movie Browsing:}} A user can also browse movies
from the current movie following its shared properties with movies,
which are represented as paths in the graph.  The choices on edges are
presented to a user when the mouse cursor is placed on a movie in the
movie browser. The choices are (1) actors, (2) directors, (3) tags,
(4) origin countries, (5) genres of the movie or (6) movies liked
(rating $\geq$ 4) by the users who liked the movie. For example, when
a user places the mouse pointer on the `Titanic' and click the left
mouse button, then the navigation menu (Fig. 3 (D)) will appear in the
browser. If the user selects 'Similar Movies-Actor', the browser will
show the movies in which the actors who acted in `Titanic' also star;
as a result, it will show movies like `Revolutionary Road'. Note that
Leonardo Dicaprio and Kate Winslet acted in both movies. Since the
number of related movies may be large, {\em PRECOS} shows similar
movies only.  The similarity between two movie $m_i$ and $m_j$ with
respect to a node type $T$, $sim_T${($m_1$,$m_2$)} is defined as the
number of distinct paths existing between $m_i$ and $m_j$ through $T$
type nodes; Let us say $m_1$, $m_2$ are of movie types representing
the movie `Titanic' and `Revolutionary Road' respectively. The
similarity $sim_{ACTOR}${($m_1$,$m_2$)} is 3, as three paths
$m_1$$\mathcal{\xrightarrow{\mathrm{\textit{starring}}}}$
$\textsf{ACTOR:}$ $\small{\textsf{`Leonardo Dicaprio'}}$
$\mathcal{\xleftarrow{\mathrm{\textit{starring}}}}$$m_1$,
$m_1$$\mathcal{\xrightarrow{\mathrm{\textit{starring}}}}$$\textsf{ACTOR:}$
$\small{\textsf{`Kate Winslet'}}$
$\mathcal{\xleftarrow{\mathrm{\textit{starring}}}}$$m_2$, and
$m_1$$\mathcal{\xrightarrow{\mathrm{\textit{starring}}}}$$\textsf{ACTOR:}$
$\small{\textsf{`Kathy Bates'}}$
$\mathcal{\xleftarrow{\mathrm{\textit{starring}}}}$$m_3$ exist in the
graph database. Note that we only counted paths only composed on
\textit{rated} type edges with positive ratings (rating $\geq$ 4) in
the case of measuring $sim_{USER}{(m_i,m_j)}$.

All user interactions with the movie browser are recorded and
converted into a selection behavior, which is stored as \textit{paths}
in \textit{Path Database}. As an example, let us assume that a user
gives rating 5 to the movie `Pulp Fiction`, and further browses movies
that are reachable from it. If he decides to browse movies with
similar tags and clicks 'Similar Movies - Tag'; as results, movies
like 'Reservoir dogs', 'Kill Bill Vol.2', and 'Fight Club' will be
presented. At this moment, the current browsing path schema tracked by
{\em PRECOS} system is $\small{\textsf{MOVIE}}$
$\mathcal{\xrightarrow{\mathrm{\textit{hasTag}}}}$
$\small{\textsf{TAG}}$
$\mathcal{\xleftarrow{\mathrm{\textit{hasTag}}}}$
$\small{\textsf{MOVIE}}$. For the sake of simplicity, we will also use an abbreviated notation (\textsf{MTM})\footnote{If we use the capital
  of the first letter of each node type name as its abbreviation
  (\textbf{U}:USER, \textbf{M}:MOVIE, \textbf{A}:ACTOR,
  \textbf{D}:DIRECTOR, \textbf{T}:TAG), we can simply denote a path as
  a sequence of letters}.

If the user continues browsing, for instance, and clicks `Similar Movies - Actor' on the movie `Fight Club' to see the
movies starring similar actors, the path schema will be updated
to $\small{\textsf{MOVIE}}$$\mathcal{\xrightarrow{\mathrm{\textit{hasTag}}}}$$\small{\textsf{TAG}}$
$\mathcal{\xleftarrow{\mathrm{\textit{hasTag}}}}$$\small{\textsf{MOVIE}}$
$\mathcal{\xrightarrow{\mathrm{\textit{starring}}}}$$\small{\textsf{ACTOR}}$$\mathcal{\xleftarrow{\mathrm{\textit{starring}}}}$$\small{\textsf{MOVIE}}$
(\textsf{\small{MAM}}). {\em PRECOS} displays the updated path schema in the movie browser (See Fig. 4). 

\begin{figure}[ht]
\center
\includegraphics[width=0.45\textwidth]{./figures/path-navigation}
\centering\caption{Whenever a user reached at a set of movies (B) by doing a
sequence of movie selection activities, PRECOS tracks and shows the user's
current movie selection history as a path schema (A). Once the user gives high
ratings to a movie among the current set of movies in the browser, the path is
stored in the \textit{Path Database}.}
\label{fig:path-navigation}
\end{figure}

If a user rates a movie with rating equal or greater than 4, a movie
selection is completed and the corresponding path is stored in the
\textit{Path Database}. PRECOS also records paths composed of the entities
specifically selected by the user along with their path-schemas. Users
can view and manage their all prior ratings and movie selection
histories by clicking `My Ratings' and `My Paths' in the menu bar
respectively.

\noindent
\textbf{\underline{Recommendation:}} {\em PRECOS}, in addition to our
path-based movie filtering, provides four other movie recommendation
algorithms: (\textit{Baseline approaches:} random, and
popularity-based, \textit{Traditional approaches:} content-based and
collaborative Filtering, and \textit{Our approach:} path-based
filtering). Brief description of each algorithms is as follows.  (1)
\textbf{random:} a set of movies is randomly selected.  (2)
\textbf{popularity-based filtering:} Movies are selected by the number
of people who gave ratings to the movies. The results are presented in
a descending order of the counts.  (3) \textbf{content-based
  filtering:} Movies that have similar tags to those of the movies
preferred by the user are selected. Formally put, if $m_i$ is a
favorite movie of the user, movies $m_j$s of which
$sim_{TAG}${($m_i$,$m_j$)} $\geq$ 4 are selected.  (4)
\textbf{collaborative-based filtering:} Movies liked by other users who also like the user's
Namely, movies with high scores of $sim_{USER}${($m_i$,$m_j$)} are selected. 
(5) \textbf{path-based filtering:} PRECOS
repetitively emulates the active user's browsing behavior by
exploiting the collected \textit{paths} and \textit{path schemas} in
the \textit{Path Database}. After a fixed number of emulations, the total visits to 
each movie is taken as the score. For example, let us assume that we
emulate a user's browsing behavior using the path schema
\textsf{\small{MTMAM}}. The detail of emulation process is described
below.


These options are presented in the drop down menu (Fig.3.
(E)) and the recommendation results are presented in the main movie
browser. 


First, PRECOS sets each movie's score to 0. Then, it repeats the following steps
for $n$ times.

\begin{enumerate}

\item PRECOS first randomly selects a movie $m_1$ among the set of
  movies previously favored by the user. The emulated path schema at this moment
  is \textsf{M}.

\item The system retrieves movies using the similarity measure
  $sim_{TAG}$, and randomly select a movie $m_2$ among them using the
  probability distribution, where the probability of an edge to
  the movie that the user previously has shown
  interest is high. At this moment,
  the emulated path schema at this moment is \textsf{MTM}.

\item It continues to the next part of the path schema and
  retrieves similar movies with $m_2$ using $sim_{ACTOR}$. Now, the whole
  path schema \textsf{MTMAM} has been emulated. The score of $m_2$ is 
  incremented by one.

\end{enumerate}

Finally, PRECOS presents movies in a descending order by their scores. When the
user selects the path-based filtering in the menu, initially, it shows the most
frequently exploited path schema on the top and generates recommendation
results using it; however, users can also generate recommendation results using
the other collected path schemas if needed.

\begin{figure}[ht]
\center
\includegraphics[width=0.45\textwidth]{./figures/path-reco}
\centering\caption{An Example of path-based recommendation: (A) is the path
schema exploited for emulating the user's movie selection browser (B) is the
recommendation results generated by PRECOS, in other words, movies that
could be found by emulating user's browsing activity using the path schema}
\label{fig:myprofile}
\end{figure}

