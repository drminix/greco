\section{Introduction}
Traditionally a movie recommendation has been considered as a problem
of finding an utility function that best predicts a user's unknown
ratings given the user's known ratings as found in
\textsf{User}$\times$\textsf{Item} rating matrix
\cite{adomavicius2005toward}.  In the past, most recommender systems,
based on this problem formulation, focused on increasing
recommendation accuracy by minimizing differences between the
predicted recommendations and the known ratings in the matrix as
exemplified by the Netflix prize \cite{bennett2007netflix}.  However,
there has been a growing observation that decrease in the errors in
the matrix alone does not necessarily increase the quality of the
recommendations, and the current trend is to incorporate external
information into the problem.

We define users' movie selection pattern as a type of external
information.  To capture a behavior, let us assume a hypothetical
environment where a user finds his/her favorable movie by completing a
sequence of decisions. For example, a user first searches for movies
in a certain category. Within the search results, the user selects a
movie and retrieves the next set of movies by choosing the movies that
are directed by the same director, then by an actress, and so on until
he/she finds a favorable movie. If it is his/her typical way of
finding a movie, we argue that the sequence represents unique
information about the user that is conducive to favorable movie
selection. 

The movie selection environment  described above is best represented as
a graph, where a node denotes movie, actor, director, etc. and an edge
denotes a relation between a pair of nodes. Then a user's movie
selection activity is a traversal over the graph through the selected
nodes and edges, that is, \textit{path}. For example, let us consider
a simple case that a user finds a favorable movie by examining the
director of a movie, and see how this would be represented in a
graph. Let $m_a$ and $m_b$ denote two nodes of type
$\small{\textsf{MOVIE}}$, and $d_a$ of type
$\small{\textsf{DIRECTOR}}$, respectively, where subscripts illustrate
specific instances. Also let
$\mathcal{\xrightarrow{\mathrm{\textit{directedBy}}}}$ denote an edge
type (or relation) between a pair of nodes that are of the types $\small{\textsf{MOVIE}}$ and
$\small{\textsf{DIRECTOR}}$, respectively. Without loss of generality, $m_a$ and $m_b$ are
instances of type $\small{\textsf{MOVIE}}$, and $d_a$ of the type $\small{\textsf{DIRECTOR}}$.
Let us assume that the user becomes
interested in the movie $\small{m_a}$ and its director $\small{d_a}$ that are linked by
an edge $\mathcal{\xrightarrow{\mathrm{\textit{directedBy}}}}$.
Then all the movies $\small{d_a}$ directed is a set of nodes that are
reachable from $\small{d_a}$ through edges of type
$\mathcal{\xrightarrow{\mathrm{\textit{directedBy}}}}$. From the movies directed by $\small{d_a}$, if $m_b$ is
a movies that the user finds attractive, a selection is completed and
the selection process is denoted by the path
$\small{\textsf{MOVIE:}}$$\small{m_a}$
$\mathcal{\xrightarrow{\mathrm{\textit{directedBy}}}}$$\small{\textsf{DIRECTOR:}}$$\small{ d_a}$$\mathcal{\xleftarrow{\mathrm{\textit{directedBy}}}}$$\small{\textsf{MOVIE:}}$$\small{m_b}$.


\begin{figure}
\center
\includegraphics[width=0.45\textwidth]{./figures/overview}
\centering\caption{Overview of PRECOS: A user's movie selection behaviors
(e.g., searching, browsing, rating) are captured by PRECOS system and exploited
for later movie recommendation }
\label{fig:overview}
\end{figure}

Often a movie selection is heavily influenced by contents of movies and choices
of others. Especially, people tends to browse movies that are either
tagged or opinioned by others. For this reason, users and tags are
integrated into the graph as nodes of type $\small{\textsf{USER}}$ and
$\small{\textsf{TAG}}$. Some path schemas that include these nodes are
$\small{\textsf{MOVIE}}$$\mathcal{\xrightarrow{\mathrm{\textit{hasTag}}}}$$\small{\textsf{TAG}}$$\mathcal{\xleftarrow{\mathrm{\textit{hasTag}}}}$$\small{\textsf{MOVIE}}$
or $\small{\textsf{MOVIE}}$$\mathcal{\xleftarrow{\mathrm{\textit{like}}}}$$\small{\textsf{USER}}$$\mathcal{\xrightarrow{\mathrm{\textit{like}}}}$$\small{\textsf{MOVIE}}$,
where former of which can interpreted as a process of content-based filtering
\cite{balabanovic1997fab} and latter of which can be interpreted as a process of
collaborative filtering \cite{sarwar2001item}.

A path on a graph is an instance of a \textit{path schema}. Whereas a
path denotes a sequence of specific nodes and edges, a path schema
represents all possible paths that have the same node and edge types
in each part of the sequences.  For example,
$\small{m_a}\mathcal{\xrightarrow{\mathrm{\textit{directedBy}}}}$$\small{d_a}$$\mathcal{\xleftarrow{\mathrm{\textit{directedBy}}}}$$\small{m_b}$
is an instance of
$\small{\textsf{MOVIE}}$$\mathcal{\xrightarrow{\mathrm{\textit{directedBy}}}}$$\small{\textsf{DIRECTOR}}$$\mathcal{\xleftarrow{\mathrm{\textit{directedBy}}}}$$\small{\textsf{MOVIE}}$.
{\em PRECOS} is a web-based movie recommender system that through
interactions collects user's movie selection patterns.  Interactions
with {\em PRECOS} include search, browse, select, rank, etc.  Each
time a user selects a movie, {\em PRECOS} converts the selection with
all intermediate interactions to a path on the graph, and creates the
corresponding path schema if not existent. {\em PRECOS} recommends
movies by emulating user's selection process using his/her path
schemas.  An emulation is a sequence of choosing a path from all the
instance paths of a path schema, where selection of each edge of a
path is a random walk process. {\em PRECOS} gives bias to certain edges in a
path schema as a user shows specific preferences in their selections. 
When a user requests a recommendation, {\em PRECOS} emulates selection
sequences numerous times for each path schema of the user and returns
movies that are chosen most. Several earlier recommendation and node
ranking approaches emphasized the importance of utilizing a graph
\cite{Fouss, Gori, lee2011random} or \textit{paths}
\cite{lee2013pathrank, PathSim}; however, none of them focused on
modeling and exploiting users' interactions with a recommender system
as \textit{paths} in the graph. Fig.~\ref{fig:overview} shows a pictorial overview of {\em PRECOS}.


The objectives of this demonstration are as follows. We show (1) how {\em
PRECOS} allows users to interactively search, browse, and rate movies via its graphical user interface
(GUI); (2) how {\em PRECOS} models and captures various user activities while
users are interacting with the system; and (2) how the system utilizes the {\em
paths} for generating various explainable recommendations. Although we
specifically focused on the movie recommendation for the purpose of
demonstration. we believe that our approach can be naturally extended for other
domains.
