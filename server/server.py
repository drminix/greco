#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import re
import time
import tornado.auth
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import unicodedata
import tornado.escape
from tornado.options import define, options
import json
import logging
import operator
import traceback
import sys
import math
import pickle
from datetime import datetime
from datetime import date
from py2neo import neo4j, node, rel
from py2neo import cypher
import random
from random import shuffle
import pickle
from collections import Counter

define("port", default=18888, help="run on the given port", type=int)
graph_db = neo4j.GraphDatabaseService("http://localhost:7474/db/data/")
neo4j._add_header('X-Stream', 'true;format=pretty')

def get_similar_movies(user_id, query_id, link_type = "*", list_size = 30, page = 1, only_unrated = False):

    user_node = graph_db.node(user_id)
    movie_list = []

    edge_type = ""
    if link_type=="*":
        edge_type = "starring|hasTag|directedBy|hasGenre|hasOriginCountry"
    elif link_type=="A":
            edge_type = "starring"
    elif link_type=="T":
            edge_type = "hasTag"
    elif link_type=="D":
            edge_type = "directedBy"
    elif link_type=="G":
            edge_type = "hasGenre"
    elif link_type=="C":
            edge_type = "hasOriginCountry"
                    
    query_str = """
    START n=node("""+str(query_id)+""") MATCH n-[r1:"""+str(edge_type)+"""]->t<-[r2:"""+str(edge_type)+"""]-m """

    if link_type=="A":
        query_str+=" WHERE r1.ranking<=2 and r2.ranking<=2 "
    query_str+= """return m, count(t) 
    as cnt order by cnt desc """

    print query_str

    if link_type=="U":
        query_str = """
        START n=node("""+str(query_id)+""") MATCH n<-[r1:rated]-u-[r2:rated]->m where r1.rating>=4.5 and r2.rating>=4.5 return m, count(u) 
        as cnt order by cnt desc """

    if page ==1:
        query_str+= """skip """+str(list_size*(page-1))+ """ limit """+str(list_size)
    else:
        query_str+= """skip """+str(list_size*(page-1)+1)+ """ limit """+str(list_size)

    query = neo4j.CypherQuery(graph_db, query_str)
    
    for record in query.execute():
        movie = record[0]
        movie_id = movie._id

        if only_unrated == True:
            if check_if_rated(user_id, movie_id) == False:
                a_movie = {}
                a_movie["_movie_id"] = movie_id
                a_movie["title"] = movie["title"]
                a_movie["year"] = movie["year"]
                a_movie["rtPictureURL"] = movie["rtPictureURL"]
                movie_list.append(a_movie)
        else:
            a_movie = {}
            a_movie["_movie_id"] = movie_id
            a_movie["title"] = movie["title"]
            a_movie["year"] = movie["year"]
            a_movie["rtPictureURL"] = movie["rtPictureURL"]
            a_movie["myrating"] = get_my_rating(user_id, movie_id)
            movie_list.append(a_movie)

        if len(movie_list)>list_size:
            break

    return movie_list

def get_my_rated_movies(user_id, login_user_id, only_liked = False, list_size = 30, page = 1):

    query_str = "START n=node("+str(user_id)+") MATCH n-[r:rated]->m "

    if only_liked == True:
        query_str+=" WHERE r.rating>=4.5 "

    query_str+=" RETURN m, r.rating as score order by score desc "

    if page ==1:
        query_str+= """skip """+str(list_size*(page-1))+ """ limit """+str(list_size)
    else:
        query_str+= """skip """+str(list_size*(page-1)+1)+ """ limit """+str(list_size)

    query = neo4j.CypherQuery(graph_db, query_str)
    cnt = 0
    list_of_movies = []
    for record in query.execute():
        movie = record[0]   
        rating = record[1]
        a_movie = {}
        a_movie["rtPictureURL"] = movie["rtPictureURL"]
        a_movie["title"] = movie["title"]
        a_movie["year"] = movie["year"]
        a_movie["_movie_id"] = movie._id
        a_movie["myrating"] = get_my_rating(login_user_id, movie._id)

        list_of_movies.append(a_movie)
    return list_of_movies  

def get_my_rating(user_id, movie_id):
    query = neo4j.CypherQuery(graph_db, "START n=node("+str(user_id)+"), m=node("+str(movie_id)+") MATCH n-[r:rated]->m RETURN m,r.rating")
    rating = 0
    for record in query.execute():
        rating = record[1]
    return rating

def get_my_ratings(user_id):
    query = neo4j.CypherQuery(graph_db, "START n=node("+str(user_id)+") MATCH n-[r:rated]->m RETURN m,r.rating")
    cnt = 0
    my_ratings = {}
    for record in query.execute():
        movie = record[0]._id    
        rating = record[1]
        my_ratings[movie]=rating
    return my_ratings    
                
def login(username):

    user_idx = graph_db.get_or_create_index(neo4j.Node, "user_idx")
    user_node = user_idx.get("username", username)
    
    user_info = {}
    if user_node !=[]:
        user_info["_username"] = username
        user_info["_user_id"] = user_node[0]._id
        return user_info
    else:
        return user_info

def create_user(username):
    
    user_idx = graph_db.get_or_create_index(neo4j.Node, "user_idx")
    user_node = user_idx.get("username", username)

    if user_node!=[]:
        return False

    else:
        
        new_user_node, = graph_db.create({"_id": "user://"+username, "userID": username})
        new_user_node.add_labels("user")
        user_idx.add("username", username, new_user_node)
        stored_paths = []
        new_user_node = user_idx.get("username", username)

        with open(str(new_user_node[0]._id)+'.path','wb') as f:
            pickle.dump(stored_paths,f)

        return True

def save_a_path(user_id, path_str):
    parsed_str = path_str.strip().split("||")
    path = {}
    path["path_str"] = path_str
    path["start_type"] = parsed_str[0][7:]
    sequence = []
    type_str = ""
    for itm in parsed_str[1:]:
        parsed_itm = itm.split("##")
        sequence.append(parsed_itm)

        if parsed_itm[0]=="movie":
            type_str+="M"
        elif parsed_itm[0]=="likeduser":
            type_str+="U"
        elif parsed_itm[0]=="country":
            type_str+="C"
        elif parsed_itm[0]=="genre":
            type_str+="G"
        elif parsed_itm[0]=="tag":
            type_str+="T"
        elif parsed_itm[0]=="actor":
            type_str+="A"
        elif parsed_itm[0]=="director":
            type_str+="D"    
    path["sequence"] = sequence
    path["path_type"] = type_str
    
    try:
        with open(str(user_id)+'.path','rb') as f:
            stored_paths=pickle.load(f)
        print stored_paths
    except:
        stored_paths = []
        print "Init: "+str(user_id)+".path"
        with open(str(user_id)+'.path','wb') as f:
            pickle.dump(stored_paths,f)

    if type_str!="M":
        stored_paths.append(path)
    with open(str(user_id)+'.path','wb') as f:
        pickle.dump(stored_paths,f)

def get_similar_user(user_id, num_of_users):
    
    #SUB FUNCTION 
    sim_user_list = []
    
    high_povot_score = 4.5
    low_povot_score = 2.5

    query_str ="""
    START u=node("""+str(user_id)+""")
    MATCH u-[r1:rated]->(m:movie)<-[r2:rated]-u2
    WHERE u2<>u and (r1.rating>="""+str(high_povot_score)+""" and r2.rating>="""+str(high_povot_score)+""")
    or (r1.rating<="""+str(low_povot_score)+"""and r2.rating<="""+str(low_povot_score)+""")
    RETURN u2, count(u2) as cnt
    order by cnt desc
    limit """+str(num_of_users)+"""
    """
    query = neo4j.CypherQuery(graph_db, query_str)
        
    for record in query.execute():
        sim_user = record[0]
        sim_user_list.append(sim_user._id)
                      
    return sim_user_list

def rate_a_movie(user_id, movie_id, rating):
    
    user_node = graph_db.node(user_id)
    movie_node = graph_db.node(movie_id)

    if check_if_rated(user_id, movie_id)==True:
        print "rating_update"
        rels = list(graph_db.match(start_node=user_node, end_node=movie_node, rel_type="rated"))
        for rel in rels:
            rel.delete()
        rel_i = graph_db.create((user_node, "rated", movie_node, {"rating":rating}))
        return False
    if (rating>=0 and rating<=5):
        rel_i = graph_db.create((user_node, "rated", movie_node, {"rating":rating}))
        return rating
    else:
        return False

def get_path_type_cnt(user_id):

    start_movies = get_my_rated_movies(user_id, user_id, only_liked = True, list_size = 100, page = 1)
    sample_num = 1000
    paths = get_paths(user_id)
    path_type_cnt = {}

    for path in paths:
        path_type = path["path_type"]
        try:
            path_type_cnt[path_type]+=1
        except:
            path_type_cnt[path_type]=1
    sorted_path_cnt = sorted(path_type_cnt.iteritems(), key=operator.itemgetter(1), reverse=True)
    return sorted_path_cnt

def path_based_recommender(user_id, path_type_str, only_unrated = True, limit = 30):
    
    start_movies = get_my_rated_movies(user_id, user_id, only_liked = True, list_size = 10000, page = 1)
    sample_num = 0
    paths = get_paths(user_id)
    
    bias_set = {}

    dest_cnt = {}

    for path in paths:
        
        path_type = path["path_type"]

        for node in path["sequence"]:
            if node[0]=='movie':
                try:
                    bias_set[path_type+"-movie"].append(node[2])
                except:
                    bias_set[path_type+"-movie"] = [node[2]]

            if node[0]=='likeduser':
                try:
                    bias_set[path_type+"-user"].append(node[2])
                except:
                    bias_set[path_type+"-user"] = [node[2]]
            if node[0]=='tag':
                try:
                    bias_set[path_type+"-tag"].append(node[2])
                except:
                    bias_set[path_type+"-tag"] = [node[2]]            
            if node[0]=='director':
                try:
                    bias_set[path_type+"-director"].append(node[2])
                except:
                    bias_set[path_type+"-director"] = [node[2]]
            if node[0]=='country':
                try:
                    bias_set[path_type+"-country"].append(node[2])
                except:
                    bias_set[path_type+"-country"] = [node[2]]
            if node[0]=='genre':
                try:
                    bias_set[path_type+"-genre"].append(node[2])
                except:
                    bias_set[path_type+"-genre"] = [node[2]]
            if node[0]=='actor':
                try:
                    bias_set[path_type+"-actor"].append(node[2])
                except:
                    bias_set[path_type+"-actor"] = [node[2]]
      
    movie_bias = []
    
    try:
        for nid in set(bias_set[path_type_str+"-actor"]):
            for movie in  get_movies(user_id, nid, 300, 1, only_unrated):
                movie_bias.append(movie['_movie_id'])
    except:
        pass
    try:
        for nid in set(bias_set[path_type_str+"-tag"]):
            for movie in  get_movies(user_id, nid, 300, 1, only_unrated):
                movie_bias.append(movie['_movie_id'])
    except:
        pass
    try:    
        for nid in set(bias_set[path_type_str+"-genre"]):
            for movie in  get_movies(user_id, nid, 300, 1, only_unrated):
                movie_bias.append(movie['_movie_id'])
    except:
        pass
    try:
        for nid in set(bias_set[path_type_str+"-director"]):
            for movie in  get_movies(user_id, nid, 300, 1, only_unrated):
                movie_bias.append(movie['_movie_id'])
    except:
        pass
    try:    
        for nid in set(bias_set[path_type_str+"-country"]):
            for movie in  get_movies(user_id, nid, 300, 1, only_unrated):
                movie_bias.append(movie['_movie_id'])
    except:
        pass
    try:   
        for nid in set(bias_set[path_type_str+"-user"]):
            for movie in  get_movies(user_id, nid, 300, 1, only_unrated):
                movie_bias.append(movie['_movie_id'])
    except:
        pass

    movie_total_list = []
    sample_cnt = 10

    for i in range(0, sample_cnt):
        init_node = start_movies[random.randint(0, len(start_movies)-1)]["_movie_id"]
        curr_node = init_node
        for path_ch in path_type_str:
            
            if path_ch!='M':
                movie_list = []
                sim_movies = get_similar_movies(user_id, curr_node, path_ch, 100, 1, False)
                if len(sim_movies)>0:        
                    for nid in sim_movies:
                        if nid['_movie_id'] in movie_bias:
                            movie_list.append(nid['_movie_id'])
                            movie_list.append(nid['_movie_id'])
                        else:
                            movie_list.append(nid['_movie_id'])

                    curr_node = movie_list[random.randint(0,len(movie_list)-1)]
                else:
                    movie_list = []

                movie_total_list+=movie_list
    
    nodes_counter = Counter(movie_total_list)
    sorted_nodes = sorted(nodes_counter.iteritems(), key=operator.itemgetter(1), reverse=True)
    return_list = []

    cnt = 0
    for tpl in sorted_nodes:
        nid = tpl[0]
        entry= graph_db.node(nid)
        if only_unrated == True:
            if check_if_rated(user_id, entry._id) == False:
                a_movie = {}
                a_movie["_movie_id"] = entry._id
                a_movie["title"] = entry["title"]
                a_movie["year"] = entry["year"]
                a_movie["rtPictureURL"] = entry["rtPictureURL"]
                a_movie["myrating"] = get_my_rating(user_id, entry._id)
                return_list.append(a_movie)
        else:
            a_movie = {}
            a_movie["_movie_id"] = entry._id
            a_movie["title"] = entry["title"]
            a_movie["year"] = entry["year"]
            a_movie["rtPictureURL"] = entry["rtPictureURL"]
            a_movie["myrating"] = get_my_rating(user_id, entry._id)
            return_list.append(a_movie)
        cnt+=1
        if cnt==limit: break

    return return_list

def path_based_recommender_old(user_id, path_type_str, only_unrated = True, limit = 30):
    
    start_movies = get_my_rated_movies(user_id, user_id, only_liked = True, list_size = 10000, page = 1)
    sample_num = 0
    paths = get_paths(user_id)
    
    bias_set = {}

    dest_cnt = {}

    for path in paths:
        
        path_type = path["path_type"]

        for node in path["sequence"]:
            if node[0]=='movie':
                try:
                    bias_set[path_type+"-movie"].append(node[2])
                except:
                    bias_set[path_type+"-movie"] = [node[2]]

            if node[0]=='likeduser':
                try:
                    bias_set[path_type+"-user"].append(node[2])
                except:
                    bias_set[path_type+"-user"] = [node[2]]
            if node[0]=='tag':
                try:
                    bias_set[path_type+"-tag"].append(node[2])
                except:
                    bias_set[path_type+"-tag"] = [node[2]]            
            if node[0]=='director':
                try:
                    bias_set[path_type+"-director"].append(node[2])
                except:
                    bias_set[path_type+"-director"] = [node[2]]
            if node[0]=='country':
                try:
                    bias_set[path_type+"-country"].append(node[2])
                except:
                    bias_set[path_type+"-country"] = [node[2]]
            if node[0]=='genre':
                try:
                    bias_set[path_type+"-genre"].append(node[2])
                except:
                    bias_set[path_type+"-genre"] = [node[2]]
            if node[0]=='actor':
                try:
                    bias_set[path_type+"-actor"].append(node[2])
                except:
                    bias_set[path_type+"-actor"] = [node[2]]
      
    sampled_cnt = {}
    
    print "\n* Sampling initiated .."
    print "----- used biases -----"
    try:
        for nid in set(bias_set[path_type_str+"-actor"]):
            bias_node= graph_db.node(nid)
            print str(bias_node["actorName"])+" ",
        print ""
    except:
        pass
    try:
        for nid in set(bias_set[path_type_str+"-tag"]):
            bias_node= graph_db.node(nid)
            print str(bias_node["value"])+" ",
        print ""
    except:
        pass
    try:    
        for nid in set(bias_set[path_type_str+"-genre"]):
            bias_node= graph_db.node(nid)
            print str(bias_node["genre"])+" ",
        print ""
    except:
        pass
    try:
        for nid in set(bias_set[path_type_str+"-director"]):
            bias_node= graph_db.node(nid)
            print str(bias_node["directorName"])+" ",
        print ""
    except:
        pass
    try:    
        for nid in set(bias_set[path_type_str+"-country"]):
            bias_node= graph_db.node(nid)
            print str(bias_node["country"])+" ",
        print ""
    except:
        pass
    try:   
        for nid in set(bias_set[path_type_str+"-user"]):
            bias_node= graph_db.node(nid)
            print str(bias_node._id)+" ",
        print ""
    except:
        pass    
    print "-----------------------"
    while sample_num<=500:
        dest = walk_a_schema_path(user_id, start_movies, path_type_str, bias_set)
        if dest != None:
            if sample_num%100==0: print "sampling paths :"+ str(sample_num)
            sample_num+=1
            #print sample_num
            try:
                sampled_cnt[graph_db.node(dest)._id]+=1
            except:
                sampled_cnt[graph_db.node(dest)._id]=1
    


    movie_list = []
    sorted_sampled_cnt = sorted(sampled_cnt.iteritems(), key=operator.itemgetter(1), reverse=True)
    cnt = 0

    for tpl in sorted_sampled_cnt:
        if only_unrated ==True:
            if check_if_rated(user_id, tpl[0])==False:
                movie = graph_db.node(tpl[0])
                a_movie = {}
                a_movie["_movie_id"] = movie._id
                a_movie["title"] = movie["title"]
                a_movie["year"] = movie["year"]
                a_movie["rtPictureURL"] = movie["rtPictureURL"]
                movie_list.append(a_movie)
        else:
            movie = graph_db.node(tpl[0])
            a_movie = {}
            a_movie["_movie_id"] = movie._id
            a_movie["title"] = movie["title"]
            a_movie["year"] = movie["year"]
            a_movie["rtPictureURL"] = movie["rtPictureURL"]
            a_movie["myrating"] = get_my_rating(user_id, movie_id)
            movie_list.append(a_movie)

        cnt+=1
        if cnt==limit: break

    return movie_list

def walk_a_schema_path(user_id, start_movies, path_type_str, bias_set):
        
    init_node = start_movies[random.randint(0, len(start_movies)-1)]["_movie_id"]

    curr_node = init_node
    for type_ch in path_type_str:
            
        if type_ch!="M":
            
            head =""
            edge_type = ""
            
            if type_ch=="A": 
                edge_type="starring"
                head ="mid=node("+str(bias_set[path_type_str+"-actor"][random.randint(0,len(bias_set[path_type_str+"-actor"])-1)])+") "
            elif type_ch=="T": 
                edge_type="hasTag"
                head ="mid=node("+str(bias_set[path_type_str+"-tag"][random.randint(0,len(bias_set[path_type_str+"-tag"])-1)])+") "
            elif type_ch=="G": 
                edge_type="hasGenre"
                head ="mid=node("+str(bias_set[path_type_str+"-genre"][random.randint(0,len(bias_set[path_type_str+"-genre"])-1)])+") "
            elif type_ch=="C": 
                edge_type="hasOriginCountry"
                head ="mid=node("+str(bias_set[path_type_str+"-country"][random.randint(0,len(bias_set[path_type_str+"-country"])-1)])+") "
            elif type_ch=="D": 
                edge_type="directedBy"
                head ="mid=node("+str(bias_set[path_type_str+"-director"][random.randint(0,len(bias_set[path_type_str+"-director"])-1)])+") "
            elif type_ch=="U":
                head ="mid=node("+str(bias_set[path_type_str+"-user"][random.randint(0,len(bias_set[path_type_str+"-user"])-1)])+") "

            if random.randint(0,100)<=80: head= """start=node("""+str(curr_node)+""")"""

            if type_ch!="U":
                query_str = """
                    START """+str(head)+""" MATCH (start:`movie`)-[:"""+edge_type+"""]->mid<-[:"""+edge_type+"""]-dest
                    WHERE dest.rtAudienceScore is not null RETURN count(dest) as max 
                """
                #print query_str
                query = neo4j.CypherQuery(graph_db, query_str)
                for record in query.execute():
                    skip_max = record[0]
                #print skip_max
            
                offset = random.randint(0,skip_max)

                query_str = """
                    START """+str(head)+""" MATCH (start:`movie`)-[:"""+edge_type+"""]->mid<-[:"""+edge_type+"""]-dest
                    WHERE dest.rtAudienceScore is not null RETURN dest, mid skip """+str(offset)+""" limit 1
                 """
            if type_ch=="U":

                query_str = """
                    START """+str(head)+""" MATCH (start:`movie`)<-[r1:rated]-mid-[r2:rated]->dest
                    WHERE dest.rtAudienceScore is not null and r1.rating>=5 and r2.rating>=5 RETURN count(dest) as max
                """
                #print query_str
                #query = neo4j.CypherQuery(graph_db, query_str)
                #for record in query.execute():
                #    skip_max = record[0]
                #print skip_max
                skip_max = 1000
                offset = random.randint(0, skip_max)

                query_str = """
                    START """+str(head)+""" MATCH (start:`movie`)<-[r1:rated]-mid-[r2:rated]->dest
                WHERE dest.rtAudienceScore is not null and r1.rating>=4 and r2.rating>=4 RETURN dest, mid skip """+str(offset)+""" limit 1
                    """
            
            cnt = 0

            if skip_max!=0:
                query = neo4j.CypherQuery(graph_db, query_str)
                for record in query.execute():
                    cnt+=1
                    movie_id = record[0]._id
                    try:
                        mid = record[1]
                    except:
                        pass

                    curr_node = movie_id
                    #print str(curr_node) +" -- by "+str(mid)
            if cnt==0:
                return None
                break

    return curr_node

def get_paths(user_id):

    try:
        with open(str(user_id)+'.path','rb') as f:
            stored_paths=pickle.load(f)
        return stored_paths
    except:
        stored_paths = []
        print "Init: "+str(user_id)+".path"
        with open(str(user_id)+'.path','wb') as f:
            pickle.dump(stored_paths,f)
        return stored_paths

def simple_recommender_cbf(user_id, list_size = 30, page = 1):

    # content-based recommender
    # num_of_users not exploited in this method

    query_str = """
    START n=node("""+str(user_id)+""")
    MATCH n-[r1:rated]->m-[:hasTag]->t<-[:hasTag]-recm
    WHERE r1.rating>=4 and recm.rtAudienceScore is not null
    WITH recm, count(recm.rtAudienceScore) as cnt
    WHERE recm.rtAudienceScore!=0
    RETURN recm, recm.rtAudienceScore/20 as score
    order by cnt desc
    """

    if page ==1:
        query_str+= """skip """+str(list_size*(page-1))+ """ limit """+str(list_size)
    else:
        query_str+= """skip """+str(list_size*(page-1)+1)+ """ limit """+str(list_size)

    query = neo4j.CypherQuery(graph_db, query_str)
    
    list_of_movies = []
    
    for record in query.execute():
        movie = record[0]
        score = record[1]
        if check_if_rated(user_id, movie._id) == False:
            a_movie = {}
            a_movie["rtPictureURL"] = movie["rtPictureURL"]
            a_movie["title"] = movie["title"]
            a_movie["year"] = movie["year"]
            a_movie["_movie_id"] = movie._id
            a_movie["predictedScore"] = score
            list_of_movies.append(a_movie)

    if list_of_movies ==[]:
        return get_random_unrated_movies(user_id, list_size = list_size)
    else:
        return list_of_movies

def simple_recommender_cf(user_id, num_of_users = 15, list_size = 30, page = 1):

    # collaborative recommender
    similar_users = get_similar_user(user_id, num_of_users)
    if similar_users == []:
        return get_random_unrated_movies(user_id, list_size = list_size)
    similar_user_str =  str(similar_users)[1:-1]
    rank = 1
    cnt = 0
    query_str = """START n=node("""+similar_user_str+""")
    MATCH n-[r1:rated]->m
    WITH m, avg(r1.rating) as avg_rating, count(m) as sup
    where sup>="""+str(num_of_users*float(0.7))+"""
    RETURN m, (avg_rating) as score 
    order by score desc
    """
    
    if page ==1:
        query_str+= """skip """+str(list_size*(page-1))+ """ limit """+str(list_size)
    else:
        query_str+= """skip """+str(list_size*(page-1)+1)+ """ limit """+str(list_size)

    query = neo4j.CypherQuery(graph_db, query_str)
    
    list_of_movies = []
    
    for record in query.execute():
        movie = record[0]
        score = record[1]
        if check_if_rated(user_id, movie._id) == False:
            a_movie = {}
            a_movie["rtPictureURL"] = movie["rtPictureURL"]
            a_movie["title"] = movie["title"]
            a_movie["year"] = movie["year"]
            a_movie["_movie_id"] = movie._id
            a_movie["predictedScore"] = score
            list_of_movies.append(a_movie)

    return list_of_movies

def check_if_rated(user_id, movie_id):
    
    query_str = """START n=node("""+str(user_id)+"""),m=node("""+str(movie_id)+""")
    MATCH n-[r1:rated]->m
    RETURN r1
    """

    query = neo4j.CypherQuery(graph_db, query_str)
    cnt = 0
    for record in query.execute():
        cnt+=1
    if cnt==0:
        return False
    else:
        return True


def search_feature_type(user_id, keywords, feature_type, only_unrated = False):

    return_list = []
    
    for keyword in keywords.split(" "):
    
        if feature_type == "movie":
            idx_str = "movie_idx"
            idx_key = "title"
        elif feature_type == "tag":
            idx_str = "tag_idx"
            idx_key = "value"
        elif feature_type == "country":
            idx_str = "country_idx"
            idx_key = "country"
        elif feature_type == "actor":
            idx_str = "actor_idx"
            idx_key = "actorName"
        elif feature_type == "genre":
            idx_str = "genre_idx"
            idx_key = "genre"
        elif feature_type == "director":
            idx_str = "director_idx"
            idx_key = "directorName"
        
        idx = graph_db.get_or_create_index(neo4j.Node, idx_str)
        nodes = idx.get(idx_key, keyword.lower())
        return_list+=nodes
    
    nodes_counter = Counter(return_list)
    sorted_nodes = sorted(nodes_counter.iteritems(), key=operator.itemgetter(1), reverse=True)
    return_list = []

    for tpl in sorted_nodes:
        entry = tpl[0]
        if feature_type == "movie":
            if only_unrated == True:
                if check_if_rated(user_id, entry._id) == False:
                    a_movie = {}
                    a_movie["_movie_id"] = entry._id
                    a_movie["title"] = entry["title"]
                    a_movie["year"] = entry["year"]
                    a_movie["rtPictureURL"] = entry["rtPictureURL"]
                    a_movie["myrating"] = get_my_rating(user_id, entry._id)
                    return_list.append(a_movie)
            else:
                a_movie = {}
                a_movie["_movie_id"] = entry._id
                a_movie["title"] = entry["title"]
                a_movie["year"] = entry["year"]
                a_movie["rtPictureURL"] = entry["rtPictureURL"]
                a_movie["myrating"] = get_my_rating(user_id, entry._id)
                return_list.append(a_movie)
        else:
            a_feature = {}
            a_feature["_nid"] = entry._id
            a_feature["_type"] = feature_type
            a_feature["_value"] = entry[idx_key]
            
            return_list.append(a_feature)

    return return_list

def search(user_id, keywords, only_unrated = False):
    
    movie_list = search_feature_type(user_id, keywords, "movie", only_unrated)
    tag_list = search_feature_type(user_id, keywords, "tag", only_unrated)
    actor_list = search_feature_type(user_id, keywords, "actor", only_unrated)
    director_list = search_feature_type(user_id, keywords, "director", only_unrated)
    genre_list = search_feature_type(user_id, keywords, "genre", only_unrated)
    country_list = search_feature_type(user_id, keywords, "country", only_unrated)

    search_result = {}
    search_result["movie"] = movie_list
    search_result["tag"] = tag_list
    search_result["actor"] = actor_list
    search_result["director"] = director_list
    search_result["genre"] = genre_list
    search_result["country"] = country_list

    return search_result

def get_movie_liked_users(user_id, movie_id, list_size=30, page =1):
    
    return_list = []

    query_str = """
    START m=node("""+str(movie_id)+""") MATCH m<-[r1:rated]-u WHERE id(u)<>"""+str(user_id)+""" and r1.rating>=4 RETURN distinct u
    """

    print query_str
    if page ==1:
        query_str+= """skip """+str(list_size*(page-1))+ """ limit """+str(list_size)
    else:
        query_str+= """skip """+str(list_size*(page-1)+1)+ """ limit """+str(list_size)

    query = neo4j.CypherQuery(graph_db, query_str)
    for record in query.execute():
        user = record[0]
        user_id = user._id
        a_user = {}
        a_user["_nid"] = user_id
        a_user["_type"] = "user"
        a_user["_value"] = user_id
        return_list.append(a_user)

    return return_list

def get_features(movie_id, feature_type, list_size = 30, page = 1):

    #feature_type = [genre, actor, director, country, tag]

    return_list = []

    query_str = """
    START m=node("""+str(movie_id)+""") MATCH m-[r1:starring|hasTag|directedBy|hasGenre|hasOriginCountry]->(f:`"""+str(feature_type)+"""`)
    RETURN distinct f, r1
    """

    if feature_type == "movie":
        value_key = "title"
    elif feature_type == "tag":
        value_key = "value"
    elif feature_type == "country":
        value_key = "country"
    elif feature_type == "actor":
        value_key = "actorName"
        query_str+=" order by r1.ranking "
    elif feature_type == "genre":
        value_key = "genre"
    elif feature_type == "director":
        value_key = "directorName"

    if page ==1:
        query_str+= """skip """+str(list_size*(page-1))+ """ limit """+str(list_size)
    else:
        query_str+= """skip """+str(list_size*(page-1)+1)+ """ limit """+str(list_size)

    query = neo4j.CypherQuery(graph_db, query_str)
    for record in query.execute():
        feature = record[0]
        feature_id = feature._id

        a_feature = {}
        a_feature["_nid"] = feature_id
        a_feature["_type"] = feature_type
        a_feature["_value"] = feature[value_key]
        return_list.append(a_feature)

    return return_list

def get_movies(user_id, query_id, list_size = 30, page = 1, only_unrated = False):

    user_node = graph_db.node(user_id)
    movie_list = []

    query_str = """
    START q=node("""+str(query_id)+""") MATCH (m:`movie`)-[:starring|hasTag|directedBy|hasGenre|hasOriginCountry]->q 
    WHERE m.rtAudienceScore is not null RETURN distinct m, m.rtAudienceScore/20 as score order by m.rtAudienceScore desc
    """

    if page ==1:
        query_str+= """skip """+str(list_size*(page-1))+ """ limit """+str(list_size)
    else:
        query_str+= """skip """+str(list_size*(page-1)+1)+ """ limit """+str(list_size)

    query = neo4j.CypherQuery(graph_db, query_str)
    for record in query.execute():
        movie = record[0]
        movie_id = movie._id

        if only_unrated == True:
            if check_if_rated(user_id, movie_id) == False:
                a_movie = {}
                a_movie["_movie_id"] = movie_id
                a_movie["title"] = movie["title"]
                a_movie["year"] = movie["year"]
                a_movie["rtPictureURL"] = movie["rtPictureURL"]
                movie_list.append(a_movie)
        else:
            a_movie = {}
            a_movie["_movie_id"] = movie_id
            a_movie["title"] = movie["title"]
            a_movie["year"] = movie["year"]
            a_movie["rtPictureURL"] = movie["rtPictureURL"]
            a_movie["myrating"] = get_my_rating(user_id, movie_id)
            movie_list.append(a_movie)

        if len(movie_list)>list_size:
            break

    return movie_list

def get_popular_unrated_movies(user_id, list_size = 30, page = 1):

    user_node = graph_db.node(user_id)
    movie_list = []
    query_str = "MATCH (m:`movie`)<-[:rated]-u RETURN m,count(u) as cnt order by cnt desc "
    if page ==1:
        query_str+= """skip """+str(list_size*(page-1))+ """ limit """+str(list_size)
    else:
        query_str+= """skip """+str(list_size*(page-1)+1)+ """ limit """+str(list_size)

    query = neo4j.CypherQuery(graph_db, query_str)
    for record in query.execute():
        movie = record[0]
        movie_id = movie._id
        if check_if_rated(user_id, movie_id) == False:
                a_movie = {}
                a_movie["_movie_id"] = movie_id
                a_movie["title"] = movie["title"]
                a_movie["year"] = movie["year"]
                a_movie["rtPictureURL"] = movie["rtPictureURL"]
                movie_list.append(a_movie)

        if len(movie_list)==list_size:
            break

    return movie_list

def get_random_unrated_movies(user_id, list_size = 30):

    user_node = graph_db.node(user_id)

    movie_list = []
    cnt = 0
    query = neo4j.CypherQuery(graph_db, "MATCH (n:`movie`) RETURN count(n)")
    for record in query.execute():
	cnt = record[0]

    while len(movie_list)<list_size:
    	rand_num = random.randint(0, cnt)
    	query = neo4j.CypherQuery(graph_db, "MATCH (n:`movie`) RETURN n skip "+str(rand_num)+" limit 1")

    	for record in query.execute():

    	    movie = record[0]
            movie_id = movie._id

            if check_if_rated(user_id, movie_id) == False:
                a_movie = {}
                a_movie["_movie_id"] = movie_id
                a_movie["title"] = movie["title"]
                a_movie["year"] = movie["year"]
                a_movie["rtPictureURL"] = movie["rtPictureURL"]
                movie_list.append(a_movie)

    return movie_list

class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
	return self.application.db

class RandomRateHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        result = get_random_unrated_movies(data['userID'][0])
        #result = get_random_unrated_movies("189598")
        self.write(tornado.escape.json_encode(result))

class RateMovieHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        print data
        result = rate_a_movie(data['userID'][0],data['movieID'][0],int(data['rating'][0]))
        #result = get_random_unrated_movies("189598")
        self.write(tornado.escape.json_encode(result))

class LoginHandler(tornado.web.RequestHandler):
    def post(self):
  
        data = self.request.arguments
        print data['userName'][0]
        result = login(data['userName'][0])
        self.write(tornado.escape.json_encode(result))

class CreateHandler(tornado.web.RequestHandler):
    def post(self):
  
        data = self.request.arguments
        #print data['userName'][0]
        result = create_user(data['userName'][0])
        #print result
        self.write(tornado.escape.json_encode(result))

class SimpleCFRecommenderHandler(tornado.web.RequestHandler):
    def post(self):
  
        data = self.request.arguments
        print data['userID'][0]
        result = simple_recommender_cf(data['userID'][0])
        self.write(tornado.escape.json_encode(result))

class SimpleCBFRecommenderHandler(tornado.web.RequestHandler):
    def post(self):
  
        data = self.request.arguments
        print data['userID'][0]
        result = simple_recommender_cbf(data['userID'][0])
        self.write(tornado.escape.json_encode(result))

class SearchHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        print "SearchHandler: "+data['keyword'][0]
        result = search(data['userID'][0], data['keyword'][0])
        self.write(tornado.escape.json_encode(result))

class GetMyRatingHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        query_user = data['userID'][0]
        login_user = query_user
        try:
            login_user = data['logUserID'][0]
        except:
            pass
        print query_user
        print login_user

        only_liked = True
        if 'only_liked' in data:
            only_liked = True
        else:
            only_liked = False
        result = get_my_rated_movies(query_user, login_user, only_liked)
        self.write(tornado.escape.json_encode(result))

class GetMoviesHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        print "GetMovies "+ data['userID'][0] + "/"+data['queryID'][0]
        result = get_movies(data['userID'][0],data['queryID'][0])

        self.write(tornado.escape.json_encode(result))

#get_popular_unrated_movies
class GetPopularHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        print "GetPopularHandler "+ data['userID'][0]
        result = get_popular_unrated_movies(data['userID'][0])

        self.write(tornado.escape.json_encode(result))

#path_based_recommender
class PathBasedRecommenderHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        print "@PathBasedRecommenderHandler "+ data['userID'][0]+","+data['path_str'][0]
        result = path_based_recommender(data['userID'][0], data['path_str'][0])

        self.write(tornado.escape.json_encode(result))

#def save_a_path(user_id, path_str):
class SavePathHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        print "SavePathHandler "+ data['userID'][0]
        result = save_a_path(data['userID'][0], data['path_str'][0])

        self.write(tornado.escape.json_encode(result))

#def save_a_path(user_id, path_str):
class GetPathHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        print "GetPathHandler "+ data['userID'][0]
        result = get_paths(data['userID'][0])

        self.write(tornado.escape.json_encode(result))

class GetFeaturesHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        combined={};
        movieID = data['movieID'][0]
        userID = data['userID'][0]
        print userID
        print "GetFeaturesHandler "+ movieID
        #actor
        result = get_features(movieID,"actor")
        combined['actor'] = result;

        #country
        result = get_features(movieID,"country");
        combined['country'] = result;

        #tag
        result = get_features(movieID,"tag");
        combined['tag'] = result;

        #director
        result = get_features(movieID,"director");
        combined['director'] = result;

        #genre
        result = get_features(movieID,"genre");
        combined['genre'] = result;

        #liked users
        result = get_movie_liked_users(userID, movieID);
        combined['likeduser'] = result;

        print combined
        self.write(tornado.escape.json_encode(combined))

class CreateHandler(tornado.web.RequestHandler):
    def post(self): 
        data = self.request.arguments
        #print data['userName'][0]
        result = create_user(data['userName'][0])
        #print result
        self.write(tornado.escape.json_encode(result))

#def get_similar_movies(user_id, query_id, link_type = "*", list_size = 30, page = 1, only_unrated = False):
class SimilarMovieHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.request.arguments
        result = get_similar_movies(data['userID'][0], data['movieID'][0], data['linkType'][0])
        self.write(tornado.escape.json_encode(result))

class Application(tornado.web.Application):
    def __init__(self):
        settings = {
        "static_path": os.path.join(os.path.dirname(__file__), "p2p")
        }
        handlers = [
        (r"/ajax/get_similar_movies", SimilarMovieHandler),
        (r"/ajax/path_based_recommender", PathBasedRecommenderHandler),
        (r"/ajax/get_path", GetPathHandler),
        (r"/ajax/save_path", SavePathHandler),
        (r"/ajax/get_popular", GetPopularHandler),
        (r"/ajax/get_features", GetFeaturesHandler),
        (r"/ajax/get_movies", GetMoviesHandler),
        (r"/ajax/get_my_rated_movies", GetMyRatingHandler),
        (r"/ajax/my_search", SearchHandler),
        (r"/ajax/rate_a_movie",RateMovieHandler),
        (r"/ajax/simple_recommender_cf", SimpleCFRecommenderHandler),
        (r"/ajax/simple_recommender_cbf", SimpleCBFRecommenderHandler),
        (r"/ajax/create", CreateHandler),
        (r"/ajax/login", LoginHandler),
        (r"/ajax/random_rate", RandomRateHandler),
        (r"/p2p/(.*)", tornado.web.StaticFileHandler, {"path": settings["static_path"] } )
        ]
        logging.basicConfig(level='error')
        logging.info("Starting server")
        tornado.web.Application.__init__(self, handlers, **settings)

def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(Application())
    http_server.listen(options.port,'0.0.0.0')
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
    
    #print get_similar_movies("190087", "116616", link_type = "U", list_size = 30, page = 1, only_unrated = False)

    # example use case of path-based recommendation for Sam Lee
    '''
    user_id = "190087"
    print path_based_recommender_new(user_id, "MDMAM", only_unrated = True)
    for path_type in get_path_type_cnt(user_id): # Iterate each saved path type order by its freqeuncy
        path_type_str = path_type[0]
        if path_type_str!="M": #path_type M shouldn't have been saved
            path_based_recommender(user_id, path_type_str, only_unrated = True) #returns movie_list in JSON
    '''
