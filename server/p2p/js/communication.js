/*
<!-- Author: Sanghyeb(Sam) Lee -->
<!-- email: slee91@utk.edu, drminix@gmail.com -->
<!-- 2014.09.04 -->

communication layer for PRECOS
*/


var debug
var _user_id;
var com_raw;
var search_raw;

//retrive random_rate movies
function get_random_movie() {
   clear_path();
   add_path("start##random");
   clear_recmovielist();
   show_loading();

  _user_id = $.session.get("_user_id");
	//create and send a request 
	    $.ajax({
        type: "post",
        url: "/ajax/random_rate",
        data: {
          userID: _user_id
        },
        success: function (data) {
       		//we have the job list, populate the combobox
         
       		movielist_raw = JSON.parse(data);

       		//received
       		console.log("get_random_rate: received "+movielist_raw.length+" lists");
     
          hide_loading();
          display_recmovielist(movielist_raw);
        } });

}

//retrive random_rate movies
function get_popular_movie() {
   clear_path();
   add_path("start##popular");
   clear_recmovielist();
   show_loading();

  _user_id = $.session.get("_user_id");
  //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/get_popular",
        data: {
          userID: _user_id
        },
        success: function (data) {
          //we have the job list, populate the combobox
         
          movielist_raw = JSON.parse(data);

          //received
          console.log("get_popular_movie: received "+movielist_raw.length+" lists");
     
          hide_loading();
          display_recmovielist(movielist_raw);
        } });

}


//retrive random_rate movies
function get_simple_recommender_cbf() {
   clear_recmovielist();
   show_loading();
   clear_path();
   add_path("start##cbf");

  _user_id = $.session.get("_user_id");
  //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/simple_recommender_cbf",
         data: {
            userID: _user_id
        },
        success: function (data) {
          
          //we have the job list, populate the combobox
          movielist_raw = JSON.parse(data);

          //received
          console.log("get_simple_recommender_cbf: received "+movielist_raw.length+" lists");
          
          hide_loading();
          display_recmovielist(movielist_raw);
        } });

}

//retrive random_rate movies
function get_simple_recommender_cf() {
    clear_path();
   add_path("start##cf");

   clear_recmovielist();
   show_loading();
  _user_id = $.session.get("_user_id");
  //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/simple_recommender_cf",
         data: {
            userID: _user_id
        },
        success: function (data) {
          
          //we have the job list, populate the combobox
          movielist_raw = JSON.parse(data);

          //received
          console.log("get_simple_recommender_cf: received "+movielist_raw.length+" lists");
          
          hide_loading();
          display_recmovielist(movielist_raw);
        } });

}

var debug_raw;

//retrive random_rate movies
function get_movies(targetID, target_svg,target_fieldset) {
   clear_recmovielist();
          show_loading();
  _user_id = $.session.get("_user_id");
  //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/get_movies",
         data: {
            userID: _user_id,
            queryID: targetID
        },
        success: function (data) {
          
          //we have the job list, populate the combobox
          local_movie_list = JSON.parse(data);
          debug_raw = local_movie_list;
          //received
          console.log("get_movies: received "+local_movie_list.length+" lists !! "+targetID);
          
          hide_loading();

          if(local_movie_list.length==0) {
            if(target_fieldset!=undefined) target_fieldset.style("display","none");

          }else {
           display_movielist(target_svg, local_movie_list, targetID);

          }
        } });

}

//retrive random_rate movies
function get_path_movies(_path_str, target_svg,target_fieldset) {
   clear_recmovielist();
   show_loading();
  _user_id = $.session.get("_user_id");
  //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/path_based_recommender",
         data: {
            userID: _user_id,
            path_str: _path_str,
        },
        success: function (data) {
          
          //we have the job list, populate the combobox
          local_movie_list = JSON.parse(data);
          debug_raw = local_movie_list;
          //received
          console.log("get_path_movies: received "+local_movie_list.length+" lists !!  for path: "+_path_str);
          
          hide_loading();

          if(local_movie_list.length==0) {
            if(target_fieldset!=undefined) target_fieldset.style("display","none");

          }else {
           display_movielist(target_svg, local_movie_list, _path_str);

          }
        } });

}


//retrive random_rate movies
function get_search(keyword) {
   clear_path();
   add_path("start##search:"+keyword);

   clear_recmovielist();
    show_loading();
  _user_id = $.session.get("_user_id");
  //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/my_search",
         data: {
            userID: _user_id,
            keyword: keyword
        },
        success: function (data) {
          
          //we have the job list, populate the combobox
          search_raw = JSON.parse(data);

          //received
          console.log("get_search: received "+search_raw+" lists");
          
          hide_loading();
          display_searchlist(search_raw);
        } });

}

//retrive random_rate movies
function get_login(username) {
  //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/login",
        data: {
            userName: username,
        },
        success: function (data) {
          //we have the job list, populate the combobox
          login_raw = JSON.parse(data);

          //received
          console.log("login: received "+data);
     
          if(!login_raw._username) {
            //if undefined, create a user
            console.log("ERROR: "+username+" does not exist!");
            console.log("Creating a new user with "+username);
            get_create_user(username);

          }else {
            //store login information
             $.session.set("_username", login_raw._username);
             $.session.set("_user_id", login_raw._user_id);
            //move onto index.html
            window.location.replace("./index.html");

          }
        } });

}

//retrive random_rate movies
function get_create_user(username) {
  //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/create",
        data: {
            userName: username,
        },
        success: function (data) {
          //we have the job list, populate the combobox
          create_raw = JSON.parse(data);

          //received
          console.log("create: received "+data);
      
          //if empty
          get_login(username);


        } });

}

function dummy(_movie_id,rating) {
  console.log("movieid: "+_movie_id+" & rating: "+rating);
}

function get_rate_a_movie(_movie_id,moviename,_rating) {

   _user_id = $.session.get("_user_id");
  //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/rate_a_movie",
        data: {
          userID: _user_id,
          movieID: _movie_id,
          rating: _rating
        },
        success: function (data) {
          //we have the job list, populate the combobox
          com_raw = JSON.parse(data);

          //received
          console.log("get_rate_a_movie: received "+com_raw);
          
          if(_rating >=4 ) { 
            var _path = $.session.get("_path");
            var recorded_path = _path + "||"+"movie##"+moviename+"##"+_movie_id;
            save_path(recorded_path);
          }
        } });


}
function get_features_for_dialog(_movie_id,location,title) {

   _user_id = $.session.get("_user_id");
  //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/get_features",
        data: {
          userID: _user_id,
          movieID: _movie_id,
        },
        success: function (data) {
          //we have the job list, populate the combobox
          com_raw = JSON.parse(data);

          //received
          console.log("get_features: received "+com_raw);

          //display dialog
         show_movie_information_step2(_movie_id,location,title,com_raw);

  
        } });

}

//get my rated movies
function get_my_rated_movies() {
   clear_path();
   add_path("start##myrating");
   clear_recmovielist();
   show_loading();
   _user_id = $.session.get("_user_id");
   //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/get_my_rated_movies",
         data: {
            userID: _user_id
        },
        success: function (data) {
          
          //we have the job list, populate the combobox
          movielist_raw = JSON.parse(data);

          //received
          console.log("get_my_rated_movies: received "+movielist_raw.length+" lists");
          
          hide_loading();
          display_recmovielist(movielist_raw);
        } });
}

//DUPLICATE
//get my rated movies
function get_my_rated_movies_liked(targetID) {
   clear_recmovielist();
   show_loading();
   _user_id = $.session.get("_user_id");
   //create and send a request 
      $.ajax({
        type: "post",
        url: "/ajax/get_my_rated_movies",
         data: {
            userID: targetID,
            logUserID: _user_id,
            only_liked: true
        },
        success: function (data) {
          
          //we have the job list, populate the combobox
          movielist_raw = JSON.parse(data);

          //received
          console.log("get_my_rated_movies: received "+movielist_raw.length+" lists");
          
          hide_loading();
          display_recmovielist(movielist_raw);
        } });
}


//hide loading
function hide_loading() {
  d3.select(".loading_marker").style("display","none");
}

//show loading
function show_loading() {
  d3.select(".loading_marker").style("display","block");
}

//clear path
function clear_path() {
  $.session.remove("_path");
}

var debug_node;
//add path
function add_path(node) {
  
  var existing_path = $.session.get("_path");
  if(existing_path==undefined) $.session.set("_path",node)
  else $.session.set("_path",existing_path+"||"+node)

  //write text
  var new_path = $.session.get("_path");
  d3.select("#browse_history").select("div").text(new_path);

  //update graph
   d3.select("#browse_history").select("svg").remove();
   d3.select("#browse_history").append("svg");
   
   draw_svg(d3.select("#browse_history").select("svg"),new_path);

}

//get path
function get_path() {
    _user_id = $.session.get("_user_id");

   //create and send a request 
    $.ajax({
      type: "post",
      url: "/ajax/get_path",
       data: {
          userID: _user_id
      },
      success: function (data) {
        
        //we have the job list, populate the combobox
        var result = JSON.parse(data);
        debug_raw = result;
        //received
        console.log("get_path: received "+result);
        
         display_profile_part2(result); 
      } });
}

//get path
function get_path_recommend() {
    _user_id = $.session.get("_user_id");
    add_path("start##path_recommend");
   //create and send a request 
    $.ajax({
      type: "post",
      url: "/ajax/get_path",
       data: {
          userID: _user_id
      },
      success: function (data) {
        
        //we have the job list, populate the combobox
        var result = JSON.parse(data);
        debug_raw = result;
        //received
        console.log("get_path: received "+result);
        
         display_path_movies_step2(result); 
      } });
}


//save path
function save_path(_path) {
   _user_id = $.session.get("_user_id");

   //create and send a request 
    $.ajax({
      type: "post",
      url: "/ajax/save_path",
       data: {
          userID: _user_id,
          path_str: _path
      },
      success: function (data) {
        
        //we have the job list, populate the combobox
        var result = JSON.parse(data);

        //received
        console.log("save_path: received "+result);
        
      } });
    
}

//SimilarMovieHandler(data['userID'][0], data['movieID'][0], data['linkType'][0])
 
function get_similar_movie(movieid,_link_type,type) {
 _user_id = $.session.get("_user_id");
  console.log("get_similar_movie: "+movieid+"/"+_link_type);

    d3.select("#my_profile").style("display","none");
    d3.select("#recmovie_list").style("display","block");
    d3.select("#search_list").style("display","none");
    d3.select("#browse_history").style("display","block");
  
  //create and send a request 
    $.ajax({
      type: "post",
      url: "/ajax/get_similar_movies",
       data: {
          userID: movieid,
          movieID: movieid,
          linkType: _link_type,
      },
      success: function (data) {
        
        //we have the job list, populate the combobox
        var result = JSON.parse(data);

        //received
        console.log("get_similar_movie: received "+result);

        //add path information
        //add parent information
        add_path(parent_information);
        add_path(type+"##"+"similar"+"##all");
        clear_recmovielist();
        display_movielist(d3.select("#recmovie_list"), result, movieid);

      } });

}
