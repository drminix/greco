/*
<!-- Author: Sanghyeb(Sam) Lee -->
<!-- email: slee91@utk.edu, drminix@gmail.com -->
<!-- 2014.09.04 -->

visualization layer for PRECOS
*/


//movie rate list 
var recmovie_list_svg_width;
var recmovie_list_svg_height;
var movie_width=300;

var movie_height=300;
var movie_padding_h=50;

var recmovie_xDim = 3;
var recmovie_yDim = 10;
var svg_recmovie;
var movielist_raw;
var xscale_level0;
var yscale_level0;
var prev_algorithm=1;

//initialize
function initialize() {
	recmovie_list_svg_width = $(window).width();
	svg_recmovie = d3.select("#recmovie_list");
    initialize_jquery();
}

function initialize_jquery() {

    // d3.select("#search_list").style("visibility","hidden");

    $( "#director_accordion" ).accordion({
      collapsible: true, active:0,heightStyle: "content" 
    });
     $( "#actor_accordion" ).accordion({
      collapsible: true, active:0,heightStyle: "content" 
    });
    $( "#tag_accordion" ).accordion({
      collapsible: true, active:0,heightStyle: "content" 
    });
     $( "#movie_accordion" ).accordion({
      collapsible: true, active:0,heightStyle: "content" 
    });
      $( "#country_accordion" ).accordion({
      collapsible: true, active:0,heightStyle: "content" 
    });
 
  

    $('#movie_menu').menu();

      $( "#movie_dialog" ).dialog({
      autoOpen: false,
      show: {
        effect: "blind",
        duration: 500
      },
      width: 500,
      hide: {
        effect: "explode",
        duration: 500
      },
    });


}

function select_algorithm(number) {
    
    //hide prev algorithm
    d3.select("#menu_algorithm"+prev_algorithm).style("visibility","hidden");

    //make it visible
    d3.select("#menu_algorithm"+number).style("visibility","visible");

    prev_algorithm = number;
       
}
function clear_recmovielist() {
    $("#recmovie_list").empty()
}

function display_path_movies() {
     //make search list visible
    d3.select("#my_profile").style("display","block");
    d3.select("#recmovie_list").style("display","none");
    d3.select("#search_list").style("display","none");
    d3.select("#browse_history").style("display","block");

     $("#my_profile").empty();
     clear_path();
    //acquire path
    get_path_recommend();
}

function display_path_movies_step2(result) {
    //for every path
    var root_div = d3.select("#my_profile");

    //process data
    var mymap = d3.map();
    for(var i=0;i<result.length;i++) {
        var element = mymap.get(result[i].path_type);
        if(element) {
            //it exists
            element.push(i);

            mymap.set(result[i].path_type, element);
        }else {
            element = [];
            element.push(i);

            mymap.set(result[i].path_type, element);
        }
    }   

  

     //display data
    var display_data = mymap.entries();

     //sort data using frequency
    display_data.sort(function(a,b) { return a.value.length<b.value.length;} )

    debug_raw2=display_data;

    for(var i=0;i<display_data.length;i++) {
        var path_type = display_data[i].key;
        //show template
        var fieldset = root_div.append("fieldset").attr("class","browserpath").attr("id","fieldset"+path_type);

        fieldset.append("legend").text("Path("+i+") :Frequency: "+display_data[i].value.length+"): "+path_type);
        var fieldset_svg = fieldset.append("svg");
        var target_index =  display_data[i].value[0];
        
        //display path
        draw_svg(fieldset_svg, result[target_index].path_str,true);
        
        //show instances
        var current_table =  fieldset.append("table");

       
        current_table.attr("class","table table-striped");
         //current_table.style("width",last_width);
        var current_tr = current_table.append("tr");

        var target_index =  display_data[i].value[0];

        //show heading
        for(var j=0;j<result[target_index].sequence.length;j++) {
            current_tr.append("td").text(result[target_index].sequence[j][0]);
        }
        
        for(var temp=0;temp<display_data[i].value.length;temp++) {
            target_index = display_data[i].value[temp];
            var current_tr = current_table.append("tr");
            for(var j=0;j<result[target_index].sequence.length;j++) {
                current_tr.append("td").text(result[target_index].sequence[j][1]);
            }
        }

        var movie_svg = fieldset.append("svg").attr("id","svg_path"+path_type);
	
    	//function get_path_movies(_path_str, target_svg,target_fieldset)
        console.log("display_path for"+path_type);

        if(i==0) get_path_movies(path_type,movie_svg,fieldset);
        else {
		    fieldset.append("br");
		    fieldset.append("button").attr("type","button").attr("class","btn btn-primary").attr("id","button"+path_type).attr("mypath",path_type)
		    .on("click", function() {
		    	var path_type = this.getAttribute("mypath");
		    	d3.select("#button"+path_type).style("display","none");
		    	var this_svg = d3.select("#svg_path"+path_type);
		    	var this_fieldset = d3.select("#fieldset"+path_type);
		    	get_path_movies(path_type,this_svg,this_fieldset);

		    }).style("width","500px").text("Press here to load movies");

    	}
        //get_path_movies(path_type,movie_svg,fieldset);

    }

}

function display_searchlist(search_list) {
    //make search list visible
    d3.select("#my_profile").style("display","none");
    d3.select("#recmovie_list").style("display","none");
    d3.select("#search_list").style("display","block");
    d3.select("#browse_history").style("display","block");
    //display movie list
    if(search_list.movie.length>0) {
         d3.select("#movie_accordion").style("display","block");
         $("#movie_list").empty();
         display_movielist(d3.select("#movie_list"), search_list.movie,"movielist");

    }else {
        d3.select("#movie_accordion").style("display","none");
        $("#movie_list").empty();
    }

    //display movies by every actor
    if(search_list.actor.length>0) {
        d3.select("#actor_accordion").style("display","block");
         $("#actor_list").empty();

        var limit=5;
        if(search_list.actor.length<limit) limit = search_list.actor.length;

        for(var i=0;i<limit;i++) {
            console.log("creating sub svg for "+search_list.actor[i]._nid);
            //create a fieldset & svg
             var target_fieldset = d3.select("#actor_list").append("fieldset").attr("class","mytitle");
             target_fieldset.append("legend").attr("class","mytitle").text(search_list.actor[i]._value);
             var target_svg = target_fieldset.append("svg");
             
             get_movies(search_list.actor[i]._nid,target_svg, target_fieldset);

            //call method by passing svg

        }
    }else {
        d3.select("#actor_accordion").style("display","none");
        $("#actor_list").empty();
    }

    //display movies by every director
    if(search_list.director.length>0) {
        d3.select("#director_accordion").style("display","block");
         $("#director_list").empty();

        var limit=5;
        if(search_list.director.length<limit) limit = search_list.director.length;

        for(var i=0;i<limit;i++) {
            console.log("creating sub svg for "+search_list.director[i]._nid);
            //create a fieldset & svg
             var target_fieldset = d3.select("#director_list").append("fieldset").attr("class","mytitle");
             target_fieldset.append("legend").attr("class","mytitle").text(search_list.director[i]._value);
             var target_svg = target_fieldset.append("svg");
             
             get_movies(search_list.director[i]._nid,target_svg, target_fieldset);

            //call method by passing svg

        }
    }else {
        d3.select("#director_accordion").style("display","none");
        $("#director_list").empty();
    }

    //display movies by every tag
    if(search_list.tag.length>0) {
        console.log("Tag");
        d3.select("#tag_accordion").style("display","block");
         $("#tag_list").empty();

        var limit=5;
        if(search_list.tag.length<limit) limit = search_list.tag.length;

        for(var i=0;i<limit;i++) {
            console.log("creating sub tag svg for "+search_list.tag[i]._nid);
            //create a fieldset & svg
             var target_fieldset = d3.select("#tag_list").append("fieldset").attr("class","mytitle");
             target_fieldset.append("legend").attr("class","mytitle").text(search_list.tag[i]._value);
             var target_svg = target_fieldset.append("svg");
             
             get_movies(search_list.tag[i]._nid,target_svg, target_fieldset);

            //call method by passing svg

        }
    }else {
        d3.select("#tag_accordion").style("display","none");
        $("#tag_list").empty();
    }

    //display movies by every tag
    if(search_list.country.length>0) {
        d3.select("#country_accordion").style("display","block");
         $("#country_list").empty();

        var limit=5;
        if(search_list.country.length<limit) limit = search_list.country.length;

        for(var i=0;i<limit;i++) {
            console.log("creating sub svg for country "+search_list.country[i]._nid);
            //create a fieldset & svg
             var target_fieldset = d3.select("#country_list").append("fieldset").attr("class","mytitle");
             target_fieldset.append("legend").attr("class","mytitle").text(search_list.country[i]._value);
             var target_svg = target_fieldset.append("svg");
             
             get_movies(search_list.country[i]._nid,target_svg, target_fieldset);

            //call method by passing svg

        }
    }else {
        d3.select("#country_accordion").style("display","none");
        $("#country_list").empty();
    }

}

var debug_y;
function display_movielist(target_svg,movie_list,list_name) {

    var local_recmovie_xDim = recmovie_xDim
    var local_recmovie_yDim = Math.ceil(movie_list.length/local_recmovie_xDim);
    
    var local_recmovie_list_svg_height = (movie_height+movie_padding_h) * local_recmovie_yDim;
    var local_recmovie_list_svg_width = recmovie_list_svg_width-200;

    xscale_level0 = d3.scale.ordinal().domain(d3.range(local_recmovie_xDim)).rangeRoundBands([0, local_recmovie_list_svg_width], 0.2);
    yscale_level0 = d3.scale.ordinal().domain(d3.range(local_recmovie_yDim)).rangeRoundBands([0, local_recmovie_list_svg_height], 0.2);
    debug_y = yscale_level0;

    target_svg.attr("width",local_recmovie_list_svg_width);
    target_svg.attr("height", local_recmovie_list_svg_height);
    

    target_svg.selectAll("foreignObject")
    .attr("class","foreign")
    .data(movie_list)
    .enter()
    .append("foreignObject")
    .attr("id", function(d,i) {
        return list_name+"movie-f"+i;
    })

    .html(function(d,i) {
        var left = xscale_level0(i%local_recmovie_xDim);
        var top = yscale_level0(Math.floor(i/local_recmovie_xDim))+(movie_height/1.5);
        
        var movieid = d._movie_id;
        var pred = parseFloat(d.predictedScore);
        var mrating = parseInt(d.myrating);
        if(!pred) pred=0; //in case, it's not defined
        if(!mrating) mrating=0;
        //when myrating is available
        var moviename=d.title+"("+d.year+")";
        
        var html =  
         "<span class=\"label label-primary\" style=\"position: relative; left:"+(left+movie_width/2-(d.title.length*2.8))+"px; top:"+(top+50)+"px; \">"+d.title+"</span><br>"
        + "<fieldset class=\"rating\" style=\"text-align:center; left:"+(left+80)+"px; top:"+top+"px; width:120px\">" 
    +"<input type=\"radio\" id=\""+list_name+i+"star52\" name=\""+i+"rating2\" value=\"5\" "+(mrating==5?"checked":"") +" onClick=\"get_rate_a_movie("+movieid+",\'"+moviename+"\',5)\"/><label for=\""+list_name+i+"star52\" title=\"Rocks!\" "+ (pred>=5?"pred":"") +">5 stars</label>"
    +"<input type=\"radio\" id=\""+list_name+i+"star42\" name=\""+i+"rating2\" value=\"4\" "+(mrating==4?"checked":"") +" onClick=\"get_rate_a_movie("+movieid+",\'"+moviename+"\',4)\"/><label for=\""+list_name+i+"star42\" title=\"Pretty good\""+ (pred>=4? "pred":"") +">4 stars</label>"
    +"<input type=\"radio\" id=\""+list_name+i+"star32\" name=\""+i+"rating2\" value=\"3\" "+(mrating==3?"checked":"") +" onClick=\"get_rate_a_movie("+movieid+",\'"+moviename+"\',3)\"/><label for=\""+list_name+i+"star32\" title=\"Meh\" "+ (pred>=3?"pred":"") +">3 stars</label>"
    +"<input type=\"radio\" id=\""+list_name+i+"star22\" name=\""+i+"rating2\" value=\"2\" "+(mrating==2?"checked":"") +" onClick=\"get_rate_a_movie("+movieid+",\'"+moviename+"\',2)\"/><label for=\""+list_name+i+"star22\" title=\"Kinda bad\" "+ (pred>=2?"pred":"") +">2 stars</label>"
    +"<input type=\"radio\" id=\""+list_name+i+"star12\" name=\""+i+"rating2\" value=\"1\" "+(mrating==1?"checked":"") +" onClick=\"get_rate_a_movie("+movieid+",\""+moviename+"\',1)\"/><label for=\""+list_name+i+"star12\" title=\"Sucks big time\" "+ (pred>=1?"pred":"") +">1 star</label>"
    +"</fieldset>"
    ;


        return html;
    });

     target_svg.selectAll("image")
    .data(movie_list)
    .enter()
    .append("image")
    .attr("id", function(d,i) {
        return list_name+"movie-i"+i;
    })
    .attr("x", function(d,i) {
        return xscale_level0(i%local_recmovie_xDim);
    })
    .attr("y", function(d,i) {
        return yscale_level0(Math.floor(i/local_recmovie_xDim));
    })
    .attr("width", movie_width)
    .attr("height", movie_height)
    .attr("visibility", "visible")
    .attr("pair", function(d,i) {
        return list_name+"movie-f"+i;
    })
    .attr("movietitle", function(d,i) {
        return d.title+"("+d.year+")";
    })
    .attr("_movie_id",function(d,i) {
        return d._movie_id;
    })
    .attr("xlink:href", function(d) {
        return d.rtPictureURL;
    }).on("click", function() {
                    //show app information in a dialog
                    //show_app_dialog(this.getAttribute("nid"),d3.mouse(this));
        //show menu on the right
        var movieid = this.getAttribute("_movie_id");
        var x = this.getAttribute("x");
        var y = this.getAttribute("y");
        var width = this.getAttribute("width");
        var height = this.getAttribute("height");
        var mouse_location = d3.mouse(this);
        var title = this.getAttribute("movietitle");
        show_movie_information(movieid,x,y,width,height,mouse_location,title);
    });
}

var parent_information;

function show_movie_information(movieid,x,y,width,height,location,title) {
    parent_information = "movie##"+title+"##"+movieid;


    console.log("!!show_movie_information: "+movieid+"/"+x+"/"+y+"/"+width+"/"+height+"/"+location[0]+"/"+location[1]);
  
    //setup display
    _title = "movie title";
    //call functions to display movie information
    get_features_for_dialog(movieid,location,title);
    //call functions to acquire related information

}

function show_movie_information_step2(movie_id,location,_title,data) {

    //display dialog
     $( "#movie_dialog" ).dialog({title: _title, 
        position: location  
    }); 

     //set genere
     if(data.genre.length==0) {
        d3.select("#movie_dialog_genere").select("a").text("Genre: unknown");
     }else if(data.genre.length==1) {

        //handle one
         d3.select("#movie_dialog_genere").select("a").attr("onClick","browse_movies("+data.genre[0]._nid+", \"genre\", \""+data.genre[0]._value+"\")").text("Genre: "+data.genre[0]._value);
         d3.select("#movie_dialog_genere").selectAll("ul").remove();
        
     }else {
         d3.select("#movie_dialog_genere").select("a").attr("aria-haspopup",true).text("Genres");
         d3.select("#movie_dialog_genere").select("a").append("span").attr("class","ui-menu-icon ui-icon ui-icon-carat-1-e");
         //<span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>
         //<a href="#" aria-haspopup="true" id="ui-id-6" class="ui-corner-all" tabindex="-1" role="menuitem"><span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>Delphi</a>
         d3.select("#movie_dialog_genere").selectAll("ul").remove();
         list = d3.select("#movie_dialog_genere").append("ul");
         list.attr("class","ui-menu ui-widget ui-widget-content ui-corner-all")
         .attr("role","menu").attr("aria-expanded","false").attr("style","display: none; top: 0px; left: 148px;").attr("aria-hidden","true");

         //<ul class="ui-menu ui-widget ui-widget-content ui-corner-all" role="menu" aria-expanded="false" style="display: none; top: 0px; left: 148px;" aria-hidden="true">
        //  <li class="ui-menu-item" role="presentation"><a href="#" id="ui-id-7" class="ui-corner-all" tabindex="-1" role="menuitem">Ada</a></li>
        

         for( var i=0;i<data.genre.length;i++) {
            list.append("li").attr("class","ui-menu-item").attr("role","presentation")
            .append("a").attr("href","#").attr("class","ui-corner-all").attr("tabindex","-1").attr("role","menuitem").attr("onClick","browse_movies("+data.genre[i]._nid+", \"genre\", \""+data.genre[i]._value+"\")").text(data.genre[i]._value);
         }
        
     }

     //set director
     tag_name = "#movie_dialog_director";

    if(data.director.length==0) {
        d3.select(tag_name).select("a").text("Director: unknown");
     }else if(data.director.length==1) {
        //handle one
         d3.select(tag_name).select("a").attr("onClick","browse_movies("+data.director[0]._nid+", \"director\", \""+data.director[0]._value+"\")").text("Director: "+data.director[0]._value);
         d3.select(tag_name).selectAll("ul").remove();
        
     }else {
        //multiple
         d3.select(tag_name).select("a").attr("aria-haspopup",true).text("Directors");
         d3.select(tag_name).select("a").append("span").attr("class","ui-menu-icon ui-icon ui-icon-carat-1-e");
         //<span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>
         //<a href="#" aria-haspopup="true" id="ui-id-6" class="ui-corner-all" tabindex="-1" role="menuitem"><span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>Delphi</a>
         d3.select(tag_name).selectAll("ul").remove();
         list = d3.select(tag_name).append("ul");
         list.attr("class","ui-menu ui-widget ui-widget-content ui-corner-all")
         .attr("role","menu").attr("aria-expanded","false").attr("style","display: none; top: 0px; left: 148px;").attr("aria-hidden","true");

         //<ul class="ui-menu ui-widget ui-widget-content ui-corner-all" role="menu" aria-expanded="false" style="display: none; top: 0px; left: 148px;" aria-hidden="true">
        //  <li class="ui-menu-item" role="presentation"><a href="#" id="ui-id-7" class="ui-corner-all" tabindex="-1" role="menuitem">Ada</a></li>
        

         for( var i=0;i<data.director.length;i++) {
            list.append("li").attr("class","ui-menu-item").attr("role","presentation")
            .append("a").attr("href","#").attr("class","ui-corner-all").attr("tabindex","-1").attr("role","menuitem").attr("onClick","browse_movies("+data.director[i]._nid+", \"director\", \""+data.director[i]._value+"\")").text(data.director[i]._value);
         }
     }

     //set tag
    tag_name = "#movie_dialog_tag";

    if(data.tag.length==0) {
        d3.select(tag_name).select("a").text("Tag: unknown");
     }else if(data.tag.length==1) {
        //handle one
         d3.select(tag_name).select("a").attr("onClick","browse_movies("+data.tag[0]._nid+", \"tag\", \""+data.tag[0]._value+"\")").text("Tag: "+data.tag[0]._value);
         d3.select(tag_name).selectAll("ul").remove();
        
     }else {
        //multiple
         d3.select(tag_name).select("a").attr("aria-haspopup",true).text("Tags");
         d3.select(tag_name).select("a").append("span").attr("class","ui-menu-icon ui-icon ui-icon-carat-1-e");
         //<span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>
         //<a href="#" aria-haspopup="true" id="ui-id-6" class="ui-corner-all" tabindex="-1" role="menuitem"><span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>Delphi</a>
         d3.select(tag_name).selectAll("ul").remove();
         list = d3.select(tag_name).append("ul");
         list.attr("class","ui-menu ui-widget ui-widget-content ui-corner-all")
         .attr("role","menu").attr("aria-expanded","false").attr("style","display: none; top: 0px; left: 148px;").attr("aria-hidden","true");

         //<ul class="ui-menu ui-widget ui-widget-content ui-corner-all" role="menu" aria-expanded="false" style="display: none; top: 0px; left: 148px;" aria-hidden="true">
        //  <li class="ui-menu-item" role="presentation"><a href="#" id="ui-id-7" class="ui-corner-all" tabindex="-1" role="menuitem">Ada</a></li>

         for( var i=0;i<data.tag.length;i++) {
            list.append("li").attr("class","ui-menu-item").attr("role","presentation")
            .append("a").attr("href","#").attr("class","ui-corner-all").attr("tabindex","-1").attr("role","menuitem").attr("onClick","browse_movies("+data.tag[i]._nid+", \"tag\", \""+data.tag[i]._value+"\")").text(data.tag[i]._value);
         }
     }




     //set actor
     tag_name = "#movie_dialog_actor";

    if(data.actor.length==0) {
        d3.select(tag_name).select("a").text("Actor: unknown");
     }else if(data.actor.length==1) {
        //handle one
         d3.select(tag_name).select("a").attr("onClick","browse_movies("+data.actor[0]._nid+", \"actor\", \""+data.actor[0]._value+"\")").text("Actor: "+data.actor[0]._value);
         d3.select(tag_name).selectAll("ul").remove();
        
     }else {
        //multiple
         d3.select(tag_name).select("a").attr("aria-haspopup",true).text("Actors");
         d3.select(tag_name).select("a").append("span").attr("class","ui-menu-icon ui-icon ui-icon-carat-1-e");
         //<span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>
         //<a href="#" aria-haspopup="true" id="ui-id-6" class="ui-corner-all" tabindex="-1" role="menuitem"><span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>Delphi</a>
         d3.select(tag_name).selectAll("ul").remove();
         list = d3.select(tag_name).append("ul");
         list.attr("class","ui-menu ui-widget ui-widget-content ui-corner-all")
         .attr("role","menu").attr("aria-expanded","false").attr("style","display: none; top: 0px; left: 148px;").attr("aria-hidden","true");

         //<ul class="ui-menu ui-widget ui-widget-content ui-corner-all" role="menu" aria-expanded="false" style="display: none; top: 0px; left: 148px;" aria-hidden="true">
        //  <li class="ui-menu-item" role="presentation"><a href="#" id="ui-id-7" class="ui-corner-all" tabindex="-1" role="menuitem">Ada</a></li>
        

         for( var i=0;i<data.actor.length;i++) {
            list.append("li").attr("class","ui-menu-item").attr("role","presentation")
            .append("a").attr("href","#").attr("class","ui-corner-all").attr("tabindex","-1").attr("role","menuitem").attr("onClick","browse_movies("+data.actor[i]._nid+", \"actor\", \""+data.actor[i]._value+"\")").text(data.actor[i]._value);
         }
     }

     //set country
    tag_name = "#movie_dialog_country";

    if(data.country.length==0) {
        d3.select(tag_name).select("a").text("Country: unknown");
     }else if(data.country.length==1) {
        //handle one
         d3.select(tag_name).select("a").attr("onClick","browse_movies("+data.country[0]._nid+", \"country\", \""+data.country[0]._value+"\")").text("Country: "+data.country[0]._value);
         d3.select(tag_name).selectAll("ul").remove();
        
     }else {
        //multiple
         d3.select(tag_name).select("a").attr("aria-haspopup",true).text("Countries");
         d3.select(tag_name).select("a").append("span").attr("class","ui-menu-icon ui-icon ui-icon-carat-1-e");
         //<span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>
         //<a href="#" aria-haspopup="true" id="ui-id-6" class="ui-corner-all" tabindex="-1" role="menuitem"><span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>Delphi</a>
         d3.select(tag_name).selectAll("ul").remove();
         list = d3.select(tag_name).append("ul");
         list.attr("class","ui-menu ui-widget ui-widget-content ui-corner-all")
         .attr("role","menu").attr("aria-expanded","false").attr("style","display: none; top: 0px; left: 148px;").attr("aria-hidden","true");

         //<ul class="ui-menu ui-widget ui-widget-content ui-corner-all" role="menu" aria-expanded="false" style="display: none; top: 0px; left: 148px;" aria-hidden="true">
        //  <li class="ui-menu-item" role="presentation"><a href="#" id="ui-id-7" class="ui-corner-all" tabindex="-1" role="menuitem">Ada</a></li>
        

         for( var i=0;i<data.country.length;i++) {
            list.append("li").attr("class","ui-menu-item").attr("role","presentation")
            .append("a").attr("href","#").attr("class","ui-corner-all").attr("tabindex","-1").attr("role","menuitem").attr("onClick","browse_movies("+data.country[i]._nid+", \"country\", \""+data.country[i]._value+"\")").text(data.country[i]._value);
         }
     }

     //set liked user
    tag_name = "#movie_dialog_likeduser";

    if(data.likeduser.length==0) {
        d3.select(tag_name).select("a").text("Liked users: 0");
     }else if(data.likeduser.length==1) {
        //handle one
         d3.select(tag_name).select("a").attr("onClick","browse_movies("+data.likeduser[0]._nid+", \"likeduser\", \""+data.likeduser[0]._value+"\")").text("Liked user: "+data.likeduser[0]._value);
         d3.select(tag_name).selectAll("ul").remove();
        
     }else {
        //multiple
         d3.select(tag_name).select("a").attr("aria-haspopup",true).text("Liked users");
         d3.select(tag_name).select("a").append("span").attr("class","ui-menu-icon ui-icon ui-icon-carat-1-e");
         //<span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>
         //<a href="#" aria-haspopup="true" id="ui-id-6" class="ui-corner-all" tabindex="-1" role="menuitem"><span class="ui-menu-icon ui-icon ui-icon-carat-1-e"></span>Delphi</a>
         d3.select(tag_name).selectAll("ul").remove();
         list = d3.select(tag_name).append("ul");
         list.attr("class","ui-menu ui-widget ui-widget-content ui-corner-all")
         .attr("role","menu").attr("aria-expanded","false").attr("style","display: none; top: 0px; left: 148px;").attr("aria-hidden","true");

         for( var i=0;i<data.likeduser.length;i++) {
            list.append("li").attr("class","ui-menu-item").attr("role","presentation")
            .append("a").attr("href","#").attr("class","ui-corner-all").attr("tabindex","-1").attr("role","menuitem").attr("onClick","browse_movies("+data.likeduser[i]._nid+", \"likeduser\", \""+data.likeduser[i]._value+"\")").text(data.likeduser[i]._value);
         }
     }

     //similar movie
	d3.select("#movie_dialog_similartag_actor").attr("onClick","get_similar_movie("+ movie_id+ ",\'A\',\'actor\')");
	d3.select("#movie_dialog_similartag_tag").attr("onClick","get_similar_movie(" + movie_id+",\'T\',\'tag\')");
	d3.select("#movie_dialog_similartag_director").attr("onClick","get_similar_movie(" + movie_id+",\'D\',\'director\')");
	d3.select("#movie_dialog_similartag_country").attr("onClick","get_similar_movie(" + movie_id+",\'C\',\'country\')");
	d3.select("#movie_dialog_similartag_user").attr("onClick","get_similar_movie(" + movie_id+ ",\'U\',\'likeduser\')");
	d3.select("#movie_dialog_similartag_genre").attr("onClick","get_similar_movie(" + movie_id+ ",\'G\',\'genre\')");
	

    $( "#movie_dialog" ).dialog( "open" );
}

function display_recmovielist(movie_list) {
    d3.select("#my_profile").style("display","none");
    d3.select("#recmovie_list").style("display","block");
    d3.select("#search_list").style("display","none");
    d3.select("#browse_history").style("display","block");

     $("#recmovie_list").empty();
     display_movielist(d3.select("#recmovie_list"), movie_list,"main_list");

    return;

 }

 function search_button() {
    console.log("sd");
    var search_keyword = $("#keyword_input").val();
    get_search(search_keyword);
 }

 function display_profile() {
    d3.select("#my_profile").style("display","block");
    d3.select("#recmovie_list").style("display","none");
    d3.select("#search_list").style("display","none");
    d3.select("#browse_history").style("display","none"); //hide it

    //remove everything
     $("#my_profile").empty();

     get_path();
 }

var debug_raw2;
 function display_profile_part2(result) {
    //for every path
    var root_div = d3.select("#my_profile");

    //process data
    var mymap = d3.map();
    for(var i=0;i<result.length;i++) {
        var element = mymap.get(result[i].path_type);
        if(element) {
            //it exists
            element.push(i);

            mymap.set(result[i].path_type, element);
        }else {
            element = [];
            element.push(i);

            mymap.set(result[i].path_type, element);
        }
    }    

   
  

     //display data
    var display_data = mymap.entries();

       //sort data using frequency
    display_data.sort(function(a,b) { return a.value.length<b.value.length;} )
    
    debug_raw2=display_data;
    for(var i=0;i<display_data.length;i++) {
        var path_type = display_data[i].key;
        //show template
        var fieldset = root_div.append("fieldset").attr("class","browserpath")
        fieldset.append("legend").text("Path("+i+"): "+path_type);
        var fieldset_svg = fieldset.append("svg");
        var target_index =  display_data[i].value[0];
        
        draw_svg(fieldset_svg, result[target_index].path_str,true);

        //show instances
        var current_table =  fieldset.append("table");

       
        current_table.attr("class","table table-striped");
         //current_table.style("width",last_width);
        var current_tr = current_table.append("tr");

        var target_index =  display_data[i].value[0];

        //show heading
        for(var j=0;j<result[target_index].sequence.length;j++) {
            current_tr.append("td").text(result[target_index].sequence[j][0]);
        }
        
        for(var temp=0;temp<display_data[i].value.length;temp++) {
            target_index = display_data[i].value[temp];
            var current_tr = current_table.append("tr");
            for(var j=0;j<result[target_index].sequence.length;j++) {
                current_tr.append("td").text(result[target_index].sequence[j][1]);
            }
        }

        //show instances
    }


 }

 function browse_movies(targetID,type,name) { 

    //add parent information
    add_path(parent_information);
    add_path(type+"##"+name+"##"+targetID);
    $( "#movie_dialog" ).dialog( "close" );
  	d3.select("#my_profile").style("display","none");
    d3.select("#recmovie_list").style("display","block");
    d3.select("#search_list").style("display","none");
    d3.select("#browse_history").style("display","block");
  

    var dummy;
    if(type=="likeduser") {
        //liked user get my rating
        get_my_rated_movies_liked(targetID);
    }else get_movies(targetID,d3.select("#recmovie_list"), dummy);
 }

 var my_nodes;
var new_path_debug;
var last_width;

function draw_svg(svg,new_path,donot_add_last) {
    new_path_debug = new_path;
  my_nodes = new_path.split("||");

  if(my_nodes.length==0) { //[] - start
    return;

  } else if(my_nodes.length==1) { //[] - start - M
    //draw one node M
    my_nodes[0]="movie";

  }else {
    // greater than one.
    my_nodes.shift();
    if(!donot_add_last) my_nodes.push("movie");
  }


  var node_radius=25;
  var node_padding=25;
  var node_offset=50;
  var color_category = d3.scale.category10();
  //nodes.shift();

  var width, height, rules, map, tasks, links, nodes, svg, tick, radius, force, link, node;
  width = 800;
  height = 50;

  rules = [];
  previous_node = "N0";
  for(var i=1;i<my_nodes.length;i++) {
     // svg.append("circle").attr("cx",((radius*2)+padding)*i+offset).attr("cy",radius).attr("r",radius).attr("fill",color_category(i));
     rules.push([previous_node,"N"+i]);
     previous_node = "N"+i;
   }

  //rules = [['P1', 'T1'], ['T1', 'P2'], ['P2', 'T2']];
  
  map = d3.map();
  rules.forEach(function(rule){
    map.set(rule[0], {
      fixed: false
    });
    return map.set(rule[1], false);
  });
  map.set("N0", {
    fixed: true,
    x: node_offset,
    y: height / 2
  });

 last_width = node_offset + (my_nodes.length*(node_radius*2+node_offset));
 console.log(last_width);

  //set
  if(my_nodes.length>1) {
      map.set(previous_node, {
        fixed: true,
        x: node_offset + (my_nodes.length*(node_radius*2+node_offset)),
        y: height / 2
      });
  }

  tasks = map.keys();
  links = rules.map(function(rule){
    return {
      source: tasks.indexOf(rule[0]),
      target: tasks.indexOf(rule[1])
    };
  });
  nodes = tasks.map(function(k){
    var entry;
    entry = {
      name: k
    };
    if (map.get(k).fixed) {
      entry.fixed = true;
      entry.x = map.get(k).x;
      entry.y = map.get(k).y;
    }
    return entry;
  });


  nodes[0].type = 'M';
  for(var i=0;i<nodes.length;i++) {
    
      if(my_nodes[i].indexOf("movie")==0) {
        nodes[i].type = 'M';
        nodes[i].typeid = 0;

      }else if(my_nodes[i].indexOf("actor")==0) {
        nodes[i].type = 'A';
        nodes[i].typeid = 1;
      }else if(my_nodes[i].indexOf("director")==0) {
        nodes[i].type = 'D';
        nodes[i].typeid = 2;
      }else if(my_nodes[i].indexOf("country")==0) {
        nodes[i].type = 'C';
        nodes[i].typeid = 3;
      }else if(my_nodes[i].indexOf("tag")==0) {
        nodes[i].type = 'T';
        nodes[i].typeid = 4;
      }else if(my_nodes[i].indexOf("likeduser")==0) {
        nodes[i].type = 'U';
        nodes[i].typeid = 4;
      }else if(my_nodes[i].indexOf("start")==0) {
        nodes[i].type = 'S';
        nodes[i].typeid = 5;
      } else if(my_nodes[i].indexOf("genre")==0) {
        nodes[i].type = 'G';
        nodes[i].typeid = 6;
      } else {
        nodes[i].type = 'E';
        nodes[i].typeid = 7;
      }

  }

  svg.attr("width",width);
  svg.attr("height",height*2);
  svg.append("svg:defs").append("svg:marker").attr("id", "arrow").attr("viewBox", "0 0 10 10").attr("refX", 27).attr("refY", 5).attr("markerUnits", "strokeWidth").attr("markerWidth", 8).attr("markerHeight", 6).attr("orient", "auto").append("svg:path").attr("d", "M 0 0 L 10 5 L 0 10 z");

  tick = function(){
    link.selectAll("line").attr("x1", function(d){
      return d.source.x;
    }).attr("y1", function(d){
      return d.source.y;
    }).attr("x2", function(d){
      return d.target.x;
    }).attr("y2", function(d){
      return d.target.y;
    }).attr("marker-end", "url(#arrow)");
    node.attr("transform", function(d){
      return "translate(" + d.x + "," + d.y + ")";
    });
  };

  radius = d3.scale.sqrt().range([0, 6]);
  force = d3.layout.force().size([width, height]).charge(-800).linkDistance(function(d){
    return 40;
  });

  force.nodes(nodes).links(links).on("tick", tick).start();

  //draw link
  link = svg.selectAll(".link").data(links).enter().append("g").attr("class", "link");
  link.append("line").style("stroke-width", 5).attr("marker-end", "url(#arrow)");

  //draw node
  color_category = d3.scale.category10();

  node = svg.selectAll(".node").data(nodes).enter().append("g").attr("class", "node").call(force.drag);
  node.append("circle").attr("r", node_radius).attr("fill", function(d,i) {
    return color_category(d.typeid);
  })
  node.append("text").attr("dy", ".35em").attr("text-anchor", "middle").text(function(d){
    return d.type;
  }).style("fill", "white");

}

